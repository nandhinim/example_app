@extends('layouts.sidebars')
@section('content')
<div class="w-full mt-10 px-20 mx-auto">
    @include('fullcalender.calendar')
</div>
@endsection