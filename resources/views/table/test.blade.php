@php
$users=collect((object)[
(object) [
'id'=>'1',
'firstname'=>'Mark',
'lastname'=>'sandy',
'email'=>'sandy@gmail.com',
],
(object) [
'id'=>'2',
'firstname'=>'Mark1',
'lastname'=>'surjith',
'email'=>'surjith@gmail.com',
],
(object) [
'id'=>'3',
'firstname'=>'Mark2',
'lastname'=>'arun',
'email'=>'arun@gmail.com',
],
(object) [
'id'=>'4',
'firstname'=>'Mark3',
'lastname'=>'nithiya',
'email'=>'nithiya@gmail.com',
],
(object) [
'id'=>'5',
'firstname'=>'Mark4',
'lastname'=>'praveen',
'email'=>'praveen@gmail.com',
]

]);



@endphp

<div class="responsive-table">
  <table id="sortable" class="table border-2 flex-row items-center justify-center p-4 mt-10  text-center">
    <thead class="">
      <tr class="ui-state-default">
        <th class=" p-4 border w-2/12 mx-auto  bg-gray-100" scope="col">#</th>
        <th class="p-4 bg-gray-100 w-3/12 mx-auto border" scope="col">First</th>
        <th class="p-4 bg-gray-100  w-3/12 mx-auto border" scope="col">Last</th>
        <th class="p-4 bg-gray-100 w-3/12 mx-auto  border" scope="col">Handle</th>
      </tr>
    </thead>
    <tbody class="">
      @foreach($users as $key=>$user)

      <tr>
        <th class="border" scope="row">{{$user->id}}</th>
        <td class="p-4 border">{{$user->firstname}}</td>
        <td class="p-4 border">{{$user->lastname}}</td>
        <td class="p-4 border">{{$user->email}}</td>
      </tr>
      @endforeach

    </tbody>
  </table>
</div>
<style>
  @import url(https://fonts.googleapis.com/css?family=Open+Sans);

  * {
    font-family: 'Open Sans', sans-serif;
  }

  .responsive-table {
    overflow: auto;
  }

  table {
    width: 100%;
    border-spacing: 0;
    border-collapse: collapse;
    white-space: nowrap;
  }

  table th {
    background: #BDBDBD;
  }

  table tr:nth-child(odd) {
    background-color: #F2F2F2;
  }

  table tr:nth-child(even) {
    background-color: #E6E6E6;
  }

  th,
  tr,
  td {
    text-align: center;
    border: 1px solid #E0E0E0;
    padding: 5px;
  }

  img {
    font-style: italic;
    font-size: 11px;
  }

  .fa-bars {
    cursor: move;
  }
</style>
<script>
  $(function() {
    $("#sortable tbody").sortable({
      cursor: "move",
      placeholder: "sortable-placeholder",
      helper: function(e, tr) {
        var $originals = tr.children();
        var $helper = tr.clone();
        $helper.children().each(function(index) {
          // Set helper cell sizes to match the original sizes
          $(this).width($originals.eq(index).width());
        });
        return $helper;
      }
    }).disableSelection();
  });
</script>