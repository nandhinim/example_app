<div class="formbold-main-wrapper">
    <!-- Author: FormBold Team -->
    <!-- Learn More: https://formbold.com -->
    <div class="formbold-form-wrapper">
        <form action="https://formbold.com/s/FORM_ID" method="POST">
            <div class="formbold-mb-5">
                <label for="name" class="formbold-form-label"> Full Name </label>
                <input type="text" name="name" id="name" placeholder="Full Name" class="formbold-form-input" />
            </div>
            <div class="formbold-mb-5">
                <label for="email" class="formbold-form-label"> Email Address </label>
                <input type="email" name="email" id="email" placeholder="Enter your email" class="formbold-form-input" />
            </div>
            <div class="formbold-mb-5">
                <label for="subject" class="formbold-form-label"> Subject </label>
                <input type="text" name="subject" id="subject" placeholder="Enter your subject" class="formbold-form-input" />
            </div>
            <div class="formbold-mb-5">
                <label for="message" class="formbold-form-label"> Message </label>
                <div class="cont">
                    <div id="editor" contenteditable="false">
                        <section id="toolbar">
                            <div id="bold" class="icon fa fa-bold"></div>
                            <div id="italic" class="icon fa fa-italic"></div>
                            <div id="createLink" class="icon fa fa-link"></div>
                            <div id="insertUnorderedList" class="icon fa fa-list"></div>
                            <div id="insertOrderedList" class="icon fa fa-list-ol"></div>
                            <div id="justifyLeft" class="icon fa fa-align-left"></div>
                            <div id="justifyRight" class="icon fa fa-align-right"></div>
                            <div id="justifyCenter" class="icon fa fa-align-center"></div>
                            <div id="justifyFull" class="icon fa fa-align-justify"></div>
                        </section>
                        <div id="page" contenteditable="true">
                            <p id="page-content"></p>
                        </div>
                        <div class="flex p-5 relative items-center">
                            <div class="mx-1 p-1 rounded" style="background-color: #f8597b;">
                                <input id="radio-1" class="radio-custom opacity-0" name="radio-group" type="radio" checked>
                                <label for="radio-1" class="radio-custom-label"></label>
                            </div>
                            <div class="mx-1 p-1 rounded" style="background-color: #fd7528;">
                                <input id="radio-1" class="radio-custom" name="radio-group" type="radio" checked>
                                <label for="radio-1" class="radio-custom-label"></label>
                            </div>
                            <div class="mx-1 p-1 rounded" style="background-color: #fda729;">
                                <input id="radio-1" class="radio-custom" name="radio-group" type="radio" checked>
                                <label for="radio-1" class="radio-custom-label"></label>
                            </div>
                            <div class="mx-1 p-1 rounded" style="background-color:#fed02f;">
                                <input id="radio-1" class="radio-custom" name="radio-group" type="radio" checked>
                                <label for="radio-1" class="radio-custom-label"></label>
                            </div>
                            <div class="mx-1 p-1 rounded" style="background-color: #ade431;">
                                <input id="radio-1" class="radio-custom" name="radio-group" type="radio" checked>
                                <label for="radio-1" class="radio-custom-label"></label>
                            </div>
                            <div class="mx-1 p-1 rounded" style="background-color: #2dd981;">
                                <input id="radio-1" class="radio-custom" name="radio-group" type="radio" checked>
                                <label for="radio-1" class="radio-custom-label"></label>
                            </div>
                            <div class="mx-1 p-1 rounded" style="background-color: #23d3c7;">
                                <input id="radio-1" class="radio-custom" name="radio-group" type="radio" checked>
                                <label for="radio-1" class="radio-custom-label"></label>
                            </div>
                            <div class="mx-1 p-1 rounded" style="background-color:#51dafb;">
                                <input id="radio-1" class="radio-custom" name="radio-group" type="radio" checked>
                                <label for="radio-1" class="radio-custom-label"></label>
                            </div>
                            <div class="mx-1 p-1 rounded" style="background-color: #1269f7;">
                                <input id="radio-1" class="radio-custom" name="radio-group" type="radio" checked>
                                <label for="radio-1" class="radio-custom-label"></label>
                            </div>
                            <div class="mx-1 p-1 rounded" style="background-color: #655df5;">
                                <input id="radio-1" class="radio-custom" name="radio-group" type="radio" checked>
                                <label for="radio-1" class="radio-custom-label"></label>
                            </div>
                            <div class="mx-1 p-1 rounded" style="background-color: #fd7dfc;">
                                <input id="radio-1" class="radio-custom" name="radio-group" type="radio" checked>
                                <label for="radio-1" class="radio-custom-label"></label>
                            </div>
                            <div class="mx-1 p-1 rounded" style="background-color: #fc50a6;">
                                <input id="radio-1" class="radio-custom" name="radio-group" type="radio" checked>
                                <label for="radio-1" class="radio-custom-label"></label>
                            </div>
                            <div class="mx-1 p-1 rounded" style="background-color: #fd94af;">
                                <input id="radio-1" class="radio-custom" name="radio-group" type="radio" checked>
                                <label for="radio-1" class="radio-custom-label"></label>
                            </div>
                            <div class="mx-1 p-1 rounded" style="background-color: #5b7895;">
                                <input id="radio-1" class="radio-custom" name="radio-group" type="radio" checked>
                                <label for="radio-1" class="radio-custom-label"></label>
                            </div>
                            <div class="mx-1 p-1 rounded" style="background-color: #f8597b;">
                                <input id="radio-1" class="radio-custom" name="radio-group" type="radio" checked>
                                <label for="radio-1" class="radio-custom-label"></label>
                            </div>
                            <div class="mx-1 p-1 rounded radio" style="background-color: #f8597b;">
                                <input id="radio-1" class="radio-custom" name="radio-group" type="radio" checked>
                                <label for="radio-1" class="radio-custom-label"></label>
                            </div>
                        </div>
                    </div>
                    <!-- <button class="log-out-client" onclick="getContent();">
                        <i class="fa fa-save"></i>
                        <span>Save content</span>
                    </button> -->
                </div>
            </div>
            <div class="mt-20">
                <button class="formbold-btn">Submit</button>
            </div>
        </form>
    </div>
</div>
<!-- colour -->
<style>
    .radio-custom {
        opacity: 0;
        position: absolute;
    }

    .fc-toolbar .fc-center {
        display: none;
    }


    .radio-custom,
    .radio-custom-label {
        display: inline-block;
        vertical-align: middle;
        /* margin: 5px; */
        padding: 4px;
        cursor: pointer;
    }

    .radio-custom-label {
        position: relative;
    }

    /* .radio-custom+.radio-custom-label:before {
        content: '';
        background: #fff;
        border: 2px solid #ddd;
        display: inline-block;
        vertical-align: middle;
        width: 20px;
        height: 20px;
        padding: 2px;
        margin-right: 10px;
        text-align: center;
    } */

    /* .checkbox-custom:checked+.checkbox-custom-label:before {
        content: "\f00c";
        font-family: 'FontAwesome';
        background: rebeccapurple;
        color: #fff;
    } */

    /* .radio-custom+.radio-custom-label:before {
        border - radius: 50%;
    } */

    .radio-custom:checked+.radio-custom-label:before {
        content: "\f00c";
        font-family: 'FontAwesome';
        color: white;
        position: absolute;
        /* top: -4px; */
        text-align: center;
        bottom: -1px;
        right: -2px;
        font-size: 11px;
    }

    .radio-custom:focus+.radio-custom-label {
        outline: 1px solid #ddd;
        /* focus style */
    }
</style>
<style>
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    body {
        font-family: "Inter", sans-serif;
    }

    .formbold-mb-5 {
        margin-bottom: 20px;
    }

    .formbold-pt-3 {
        padding-top: 12px;
    }

    .formbold-main-wrapper {
        display: flex;
        align-items: center;
        justify-content: center;
        padding: 48px;
    }

    .formbold-form-wrapper {
        margin: 0 auto;
        /* max-width: 550px; */
        width: 100%;
        background: white;
        padding: 52px;
    }

    .formbold-form-label {
        display: block;
        font-weight: 500;
        font-size: 16px;
        color: #07074d;
        margin-bottom: 12px;
    }

    .formbold-form-label-2 {
        font-weight: 600;
        font-size: 20px;
        margin-bottom: 20px;
    }

    .formbold-form-input {
        width: 100%;
        padding: 12px 24px;
        border-radius: 6px;
        border: 1px solid #e0e0e0;
        background: white;
        font-weight: 500;
        font-size: 16px;
        color: #6b7280;
        outline: none;
        resize: none;
    }

    .formbold-form-input:focus {
        border-color: #6a64f1;
        box-shadow: 0px 3px 8px rgba(0, 0, 0, 0.05);
    }

    .formbold-btn {
        text-align: center;
        font-size: 16px;
        border-radius: 6px;
        padding: 14px 32px;
        border: none;
        font-weight: 600;
        background-color: #6a64f1;
        color: white;
        cursor: pointer;
    }

    .formbold-btn:hover {
        box-shadow: 0px 3px 8px rgba(0, 0, 0, 0.05);
    }

    .formbold--mx-3 {
        margin-left: -12px;
        margin-right: -12px;
    }

    .formbold-px-3 {
        padding-left: 12px;
        padding-right: 12px;
    }

    .flex {
        display: flex;
    }

    .flex-wrap {
        flex-wrap: wrap;
    }

    .w-full {
        width: 100%;
    }

    @media (min-width: 540px) {
        .sm\:w-half {
            width: 50%;
        }
    }

    @import url('https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i');

    * {
        margin: 0;
        padding: 0;
        outline: none !important;
        text-decoration: none !important;
        box-sizing: border-box;
        line-height: initial;
        letter-spacing: initial;
        font-family: 'Roboto', sans-serif;
    }

    a {
        color: #3F51B5;
    }

    a:hover {
        color: #333333;
    }

    body {
        background-color: #f1f1f1;
        color: #333;
        font-family: 'Roboto', sans-serif;
    }

    button.log-out-client {
        height: 45px;
        border: 0;
        padding-right: 20px;
        border-radius: 4px;
        background-color: #3F51B5;
        color: rgba(255, 255, 255, 0.8);
        line-height: 43px;
        cursor: pointer;
        transition: background-color 0.4s;
    }

    button.log-out-client i {
        width: 45px;
        line-height: 45px;
        height: 45px;
        background-color: rgba(0, 0, 0, 0.1);
        margin-right: 15px;
        color: rgba(255, 255, 255, 0.7);
    }

    button.log-out-client:hover,
    button.log-out-client.cancel:hover {
        background-color: #333;
    }

    #editor {
        width: 800px;
        margin: 50px auto 50px auto;
        padding: 7px 20px 20px 20px;
        border: 1px solid #464646;
        border-radius: 10px;
        background: #fafafa;
    }

    #toolbar {
        margin-left: -20px;
        margin-right: -20px;
        border-bottom: 1px solid #464646;
        padding: 8px 20px;
        overflow: hidden;
        position: relative;
        z-index: 2;
    }

    #page {
        padding-top: 30px;
        min-height: 600px;
        outline: none;
    }

    .icon {
        float: left;
        height: 30px;
        width: 40px;
        margin-right: 10px;
        background: #fafafa;
        font-size: 0.8rem;
        color: #464646;
        text-align: center;
        cursor: pointer;
    }

    .icon:hover {
        border-color: #3e7086;
        color: #3e7086;
    }

    div#editor {
        width: 100%;
        height: 250px;
        margin: 0;
        margin-bottom: 20px;
        padding: 0;
        border: 1px solid #cfcfcf;
        border-radius: 4px;
    }

    #page * {
        font-family: 'Roboto', sans-serif;
    }

    #page {
        min-height: initial;
        height: 100%;
        margin: 0;
        box-sizing: border-box;
        padding: 10px;
        padding-top: 63px;
        margin-top: -53px;
        text-align: left;
        position: relative;
        z-index: 1;
    }

    section#toolbar {
        height: 53px;
        padding: 10px;
        margin: 0;
        border-bottom: 1px solid #cfcfcf;
        background-color: #fff;
        overflow: hidden;
        border-radius: 4px 4px 0px 0px;
        padding-left: 15px;
        padding-right: 15px;
    }

    .icon {
        height: 33px;
        background: transparent;
        opacity: 0.7;
        color: #555;
        width: 33px;
        line-height: 33px;
    }

    .icon:hover {
        opacity: 1;
        color: #3F51B5;
    }

    ul,
    ol {
        padding-left: 28px;
        color: #777;
    }

    body {
        width: 100%;
        height: 100%;
        min-height: 100vh;
        display: flex;
    }

    .cont {
        width: 600px;
        max-width: 96%;
        margin: auto;
    }
</style>
<script>
    var ghostEditor = {
        bindEvents: function() {
            this.bindDesignModeToggle();
            this.bindToolbarButtons();
        },

        bindDesignModeToggle: function() {
            $('#page-content').on('click', function(e) {
                document.designMode = 'on';
            });

            $('#page-content').on('click', function(e) {
                var $target = $(e.target);

                if ($target.is('#page-content')) {
                    document.designMode = 'off';
                }
            });
        },

        bindToolbarButtons: function() {
            $('#toolbar').on('mousedown', '.icon', function(e) {
                e.preventDefault();
                var btnId = $(e.target).attr('id');
                this.editStyle(btnId);
            }.bind(this));
        },

        editStyle: function(btnId) {
            var value = null;

            if (btnId === 'createLink') {
                if (this.isSelection()) value = prompt('Enter a link:');
            }

            document.execCommand(btnId, true, value);
        },

        isSelection: function() {
            var selection = window.getSelection();
            return selection.anchorOffset !== selection.focusOffset
        },

        init: function() {
            this.bindEvents();
        },
    }

    ghostEditor.init();

    function getContent() {
        var content = document.getElementById('page').innerHTML;
        alert(content);
    }
</script>