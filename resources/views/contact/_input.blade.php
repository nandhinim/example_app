<div class="lg:w-6/12 md:w-full w-full lg:p-20 md:p-20 p-0 bg-white shadow rounded">
    <div class="pt-0">
        <div class="">
            <p>CONTACT<span class="text-teal-500 mx-2" style="top:0; font-size:18px;">FORM</span></p>
        </div>
        <div class="">
            <input type="text" placeholder="Your Name*" class="border w-full py-2 px-2 mt-10">
            <input type="text" placeholder="Your Email*" class="border w-full py-2 px-2 mt-10">
            <input type="text" placeholder="Subject*" class="border w-full py-2 px-2 mt-10">
            <textarea rows="4" cols="50" class="border w-full mt-10 "></textarea>
        </div>
        <div class="mt-10">
            <a href="#" class="px-6 py-2 bg-teal-500 rounded text-white">SUBMIT</a>
        </div>
    </div>
</div>