<div class="p-10 lg:w-6/12 md:w-full w-full">
    <div class="pt-10  ">
        <div class="">
            <p>OUR <span class="text-teal-500 mx-2" style="top:0; font-size:18px;">LOCATION</span></p>
        </div>
        <div class="pt-10">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62575.03261379275!2d76.65840173777164!3d11.411934660380583!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba8bd84b5f3d78d%3A0x179bdb14c93e3f42!2sOoty%2C%20Tamil%20Nadu!5e0!3m2!1sen!2sin!4v1661233786113!5m2!1sen!2sin" width="500" height="300" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        </div>
        <div class="">
            <div class="pt-10">
                <p>CONTACT <span class="text-teal-500 mx-2" style="top:0; font-size:18px;">DETAILS</span></p>
            </div>
            <div class="pt-5">
                <ul class="text-sm">
                    <li class="flex pt-2">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 text-teal-500 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M12 18h.01M8 21h8a2 2 0 002-2V5a2 2 0 00-2-2H8a2 2 0 00-2 2v14a2 2 0 002 2z" />
                        </svg>
                        <p class="mx-4">+ 357 720 1443</p>
                    </li>
                    <li class="flex pt-4">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 text-teal-500 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                        </svg>
                        <p class="mx-4">contact@company.com</p>
                    </li>
                    <li class="flex pt-4">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 text-teal-500 w-6" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M5.05 4.05a7 7 0 119.9 9.9L10 18.9l-4.95-4.95a7 7 0 010-9.9zM10 11a2 2 0 100-4 2 2 0 000 4z" clip-rule="evenodd" />
                        </svg>
                        <p class="mx-4">362 52nd Street, New York City, NY</p>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</div>