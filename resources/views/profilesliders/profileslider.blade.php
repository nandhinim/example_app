<div class="owl-slider">
    <div id="carousel1" class="owl-carousel">
        <div class="item w-6/12 mx-auto p-5 item-center justify-center">
            <div class="flex items-center justify-center">
                <img class=" h-28 rounded-full " src="images/alen.jpg" style="width: 22%;">
            </div>
            <div class="text-center pt-6">
                <p class="font-bold text-2xl">DAVID S. MORRIS</p>
            </div>
            <div class="text-center pt-4">
                <p class="text-xl">Amet, consectetur adipisicing elit, sed do eiusmod incididunt ut<br>labore et
                    dolore magna
                    aliqua.Ut enim minim veniam</p>
            </div>
        </div>
        <div class="item w-6/12 mx-auto  p-5" style="width: 9%;">
            <div class="flex items-center justify-center">
                <img class=" h-28 rounded-full" src="images/Alan-Smith.jpg" style="width: 22%;">
            </div>
            <div class="text-center pt-6">
                <p class="font-bold text-2xl">DALMAR JOHNSON</p>
            </div>
            <div class="text-center pt-4">
                <p class="text-xl">Amet, consectetur adipisicing elit, sed do eiusmod incididunt ut<br>labore et
                    dolore magna
                    aliqua.Ut enim minim veniam</p>
            </div>
        </div>
        <div class="item w-6/12 mx-auto  p-5" style="width: 9%;">
            <div class="flex items-center justify-center">
                <img class=" h-28 rounded-full" src="images/aisy.jpg" style="width: 22%;">
            </div>
            <div class="text-center pt-6">
                <p class="font-bold text-2xl">RICHARD M. BLALOCK</p>
            </div>
            <div class="text-center pt-4">
                <p class="text-xl">Amet, consectetur adipisicing elit, sed do eiusmod incididunt ut<br>labore et
                    dolore magna
                    aliqua.Ut enim minim veniam</p>
            </div>
        </div>
    </div>
</div>
</div>
<style>
    .owl-nav button {
        /* position: absolute; */
        top: 50%;
        color: #fff;
        margin: 0;
        transition: all 0.3s ease-in-out;
    }

    .owl-nav button.owl-prev {
        left: 0;
        width: 4%;
        margin: auto;

    }

    .owl-nav {
        /* width: 22%; */
        /* margin: auto; */
        text-align: center;
    }

    .owl-nav button.owl-next {
        right: 0;
        margin: auto;

    }

    /* .owl-dots {
        text-align: center;
        padding-top: 15px;
    } */

    /* .owl-dots button.owl-dot {
        width: 15px;
        height: 15px;
        display: inline-block;
        background: #ccc;
        margin: 0 3px;
    } */

    .owl-dots button.owl-dot.active {
        background-color: #000;
    }

    .owl-dots button.owl-dot:focus {
        outline: none;
    }

    .owl-nav button {
        /* position: absolute; */
        top: 50%;
        /* transform: translateY(-50%); */
        background: rgba(255, 255, 255, 0.38) !important;
    }

    span {
        font-size: 70px;
        /* position: relative; */
        top: 69px;

    }

    .owl-nav button:focus {
        outline: none;
    }
</style>
<script>
    jQuery("#carousel1").owlCarousel({
        autoplay: false,
        rewind: true,
        /* use rewind if you don't want loop */
        margin: 20,
        /*
       animateOut: 'fadeOut',
       animateIn: 'fadeIn',
       */
        responsiveClass: true,
        autoHeight: true,
        autoplayTimeout: 7000,
        smartSpeed: 800,
        nav: true,
        responsive: {
            0: {
                items: 1
            },

            600: {
                items: 1
            },

            1024: {
                items: 1
            },

            1366: {
                items: 1
            }
        }
    });
</script>