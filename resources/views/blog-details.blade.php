@extends('layouts.app')
@section('content')
<div class="bg-gray-200 shadow">
    <div class="container  mx-auto">
        <div class="lg:flex-row md:flex-col flex-col flex p-10">
            <div class="text-gray-400">
                <p>LATEST NEWS POST</p>
            </div>
            <div class="ml-auto flex items-center justify-center">
                <ul class="flex items-center justify-center text-xs text-gray-400">
                    <li class="flex items-center justify-center">
                        <p>Home</p>
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 text-gray-400 w-4" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                        </svg>
                    </li>
                    <li class="flex items-center justify-center">
                        <p>Mobile Apps</p>
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 text-gray-400 w-4" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                        </svg>
                    </li>
                    <li class="flex items-center justify-center">
                        <p>Brand Identity</p>
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 text-gray-400 w-4" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                        </svg>
                    </li>
                    <li class="flex items-center justify-center">
                        <p>Marketing</p>
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 text-gray-400 w-4" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                        </svg>
                    </li>
                    <li class="">
                        <p>Latest News Post</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container mx-auto">
    <div class="lg:flex-row flex-col flex-col items-center justify-center flex lg:p-20 md:p-0 p-0">
        @include('.blog-details._left')
        @include('.blog-details._right')
    </div>
</div>
@endsection