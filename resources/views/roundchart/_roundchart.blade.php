<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<main>
    <h5>Page Hits per Country</h5>
    <div id="pie-chart"></div>
</main>
<style>
    body {
        font-family: "Open Sans", Arial;
        background: #EDEDED;
    }

    main {
        width: 500px;
        margin: 10px auto;
        padding: 10px 20px 30px;
        background: #FFF;
        box-shadow: 0 3px 5px rgba(0, 0, 0, 0.2);
    }

    /* p {
        margin-top: 2rem;
        font-size: 13px;
    } */

    #pie-chart {
        width: 500px;
        height: 250px;
        position: relative;
    }

    #pie-chart::before {
        content: "";
        position: absolute;
        display: block;
        width: 120px;
        height: 115px;
        left: 315px;
        top: 0;
        background: #FAFAFA;
        box-shadow: 1px 1px 0 0 #DDD;
    }

    #pie-chart::after {
        content: "";
        position: absolute;
        display: block;
        top: 260px;
        left: 70px;
        width: 170px;
        height: 2px;
        background: rgba(0, 0, 0, 0.1);
        border-radius: 50%;
        box-shadow: 0 0 3px 4px rgba(0, 0, 0, 0.1);
    }
</style>
<script>
    google.load("visualization", "1", {
        packages: ["corechart"]
    });
    google.setOnLoadCallback(drawCharts);

    function drawCharts() {
        // pie chart data
        var pieData = google.visualization.arrayToDataTable([
            ['Country', 'Page Hits'],
            ['USA', 7242],
            ['Canada', 4563],
            ['Mexico', 1345],
            ['Sweden', 946],
            ['Germany', 2150]
        ]);
        // pie chart options
        var pieOptions = {
            backgroundColor: 'transparent',
            pieHole: 0.4,
            colors: ["cornflowerblue",
                "olivedrab",
                "orange",
                "tomato",
                "crimson",
                "purple",
                "turquoise",
                "forestgreen",
                "navy",
                "gray"
            ],
            pieSliceText: 'value',
            tooltip: {
                text: 'percentage'
            },
            fontName: 'Open Sans',
            chartArea: {
                width: '100%',
                height: '94%'
            },
            legend: {
                textStyle: {
                    fontSize: 13
                }
            }
        };
        // draw pie chart
        var pieChart = new google.visualization.PieChart(document.getElementById('pie-chart'));
        pieChart.draw(pieData, pieOptions);
    }
</script>