<div class="">
    <div class="c-sidebar bg-gray-100 shadow">
        <div class="p-20">
            <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
        </div>
    </div>
    <div class="c-main ">
        <div class="p-10">
            <p class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis dolores reiciendis molestiae assumenda excepturi fugit, recusandae illo deleniti ratione delectus, magni pariatur, natus. Quo, molestiae sequi veritatis deserunt dolore dolores.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis dolores reiciendis molestiae assumenda excepturi fugit, recusandae illo deleniti ratione delectus, magni pariatur, natus. Quo, molestiae sequi veritatis deserunt dolore dolores. Quis dolores reiciendis molestiae assumenda excepturi fugit, recusandae illo deleniti ratione delectus, magni pariatur, natus. Quo, molestiae sequi veritatis deserunt dolore dolores.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis dolores reiciendis molestiae assumenda excepturi fugit, recusandae illo deleniti ratione delectus, magni pariatur, natus. Quo, molestiae sequi veritatis deserunt dolore dolores.</p>
        </div>
    </div>
</div>
<style>
    .c-sidebar {
        width: 44%;
        float: left;
        margin: auto;

    }

    html,
    body,
    p {
        margin: 0;
        padding: 0;
    }

    html,
    body,
    .c-main,
    .c-sidebar {
        height: 100%;
    }

    /* 
    .c-sidebar {
        background-color: gray;
    } */
</style>
<script>
    var isResizeable = interact('.c-sidebar').resizable({
        axis: 'x',
        enabled: true
    }).on('resizemove', function(event) {
        var target = event.target;
        var w = $(target).width() + event.dx;
        target.style.width = w + 'px';
    });
</script>