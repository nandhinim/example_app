@extends('layouts.app')
@section('content')
  @include('home._slider')
   @include('home._icon')
   @include('home._iphone')
   @include('home._stunning_design')
   @include('home._partners_clients')
   @include('home._slow')
   @include('home._tech_spec')
   @include('home._slider2')
   @include('home._overlay') 
   @include('home._croft')
   @include('home._wordpress')
   @include('home._explore_create')
   @include('home._awesome')
   @include('home._3color')
   @include('home._responsive')
   @include('home._fortuna')
   @include('home._latest_news')
@endsection