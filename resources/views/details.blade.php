@extends('layouts.app')
@section('content')
@include('.details._bg')
<div class="lg:flex md:flex-card flex-card">
    @include('.details._leftslider')
    @include('.details._rightcondent')
</div>
@include('.details._relatedproducts')
@endsection