<div class="container mx-auto">
    <div class="w-1/4 mx-auto border-2">
        <div class="bg-white">
            <div class="flex justify-between p-5">
                <div class="flex  items-center text-black">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-4 font-bold h-4">
                        <path fill-rule="evenodd" d="M7.28 7.72a.75.75 0 010 1.06l-2.47 2.47H21a.75.75 0 010 1.5H4.81l2.47 2.47a.75.75 0 11-1.06 1.06l-3.75-3.75a.75.75 0 010-1.06l3.75-3.75a.75.75 0 011.06 0z" clip-rule="evenodd" />
                    </svg>
                    <p class="mx-4 text-xl font-bold">Product</p>
                </div>
                <div class=" bg-blue-100 p-2 rounded">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-4 h-4">
                        <path fill-rule="evenodd" d="M3.792 2.938A49.069 49.069 0 0112 2.25c2.797 0 5.54.236 8.209.688a1.857 1.857 0 011.541 1.836v1.044a3 3 0 01-.879 2.121l-6.182 6.182a1.5 1.5 0 00-.439 1.061v2.927a3 3 0 01-1.658 2.684l-1.757.878A.75.75 0 019.75 21v-5.818a1.5 1.5 0 00-.44-1.06L3.13 7.938a3 3 0 01-.879-2.121V4.774c0-.897.64-1.683 1.542-1.836z" clip-rule="evenodd" />
                    </svg>

                </div>
            </div>
            <div class="p-5" x-data="{showAccordion:0}">
                <ul>
                    <li>
                        <div class="flex justify-between rounded items-center p-2 custom-input" :class="{'bg-blue-100' : showAccordion == 1}" @click="showAccordion = 1">
                            <a href="#" class="text-xs">Industrial Glass Slab 12mm</a>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-3 h-3">
                                <path fill-rule="evenodd" d="M12.53 16.28a.75.75 0 01-1.06 0l-7.5-7.5a.75.75 0 011.06-1.06L12 14.69l6.97-6.97a.75.75 0 111.06 1.06l-7.5 7.5z" clip-rule="evenodd" />
                            </svg>
                        </div>
                        <div class="bg-blue-100 p-5" x-show="showAccordion == 1">
                            <img class="w-40 h-28" src="images/1.jpg">
                        </div>
                    </li>
                    <li class="mt-4">
                        <div class="flex justify-between rounded items-center p-2 custom-input" :class="{'bg-blue-100' : showAccordion == 2}" @click="showAccordion = 2">
                            <a href="#" class="text-xs">Scientific hallow glass</a>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-3 h-3">
                                <path fill-rule="evenodd" d="M12.53 16.28a.75.75 0 01-1.06 0l-7.5-7.5a.75.75 0 011.06-1.06L12 14.69l6.97-6.97a.75.75 0 111.06 1.06l-7.5 7.5z" clip-rule="evenodd" />
                            </svg>
                        </div>
                        <div class="bg-blue-100 p-5" x-show="showAccordion == 2">
                            <img class="w-40 h-28" src="images/2.jpg">
                        </div>
                    </li>
                    <li class="mt-4">
                        <div class="flex justify-between rounded items-center p-2 custom-input" :class="{'bg-blue-100' : showAccordion == 3}" @click="showAccordion = 3">
                            <a href="#" class="text-xs">Domestic glass</a>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-3 h-3">
                                <path fill-rule="evenodd" d="M12.53 16.28a.75.75 0 01-1.06 0l-7.5-7.5a.75.75 0 011.06-1.06L12 14.69l6.97-6.97a.75.75 0 111.06 1.06l-7.5 7.5z" clip-rule="evenodd" />
                            </svg>
                        </div>
                        <div class="bg-blue-100 p-5" x-show="showAccordion == 3">
                            <img class="w-40 h-28" src="images/3.jpg">
                        </div>
                    </li>
                    <li class="mt-4">
                        <div class="flex justify-between rounded items-center p-2 custom-input" :class="{'bg-blue-100' : showAccordion == 4}" @click="showAccordion = 4">
                            <a href="#" class="text-xs">Glass fiber</a>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-3 h-3">
                                <path fill-rule="evenodd" d="M12.53 16.28a.75.75 0 01-1.06 0l-7.5-7.5a.75.75 0 011.06-1.06L12 14.69l6.97-6.97a.75.75 0 111.06 1.06l-7.5 7.5z" clip-rule="evenodd" />
                            </svg>
                        </div>
                        <div class="bg-blue-100 p-5" x-show="showAccordion == 4">
                            <img class="w-40 h-28" src="images/4.jpg">
                        </div>
                    </li>
                    <li class="mt-4">
                        <div class="flex justify-between rounded items-center p-2 custom-input" :class="{'bg-blue-100' : showAccordion == 5}" @click="showAccordion = 5">
                            <a href="#" class="text-xs">Industrial Glass Slab 12mm</a>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-3 h-3">
                                <path fill-rule="evenodd" d="M12.53 16.28a.75.75 0 01-1.06 0l-7.5-7.5a.75.75 0 011.06-1.06L12 14.69l6.97-6.97a.75.75 0 111.06 1.06l-7.5 7.5z" clip-rule="evenodd" />
                            </svg>
                        </div>
                        <div class="bg-blue-100 p-5" x-show="showAccordion == 5">
                            <img class="w-40 h-28" src="images/1.jpg">
                        </div>
                    </li>
                    <li class="mt-4">
                        <div class="flex justify-between rounded items-center p-2 custom-input" :class="{'bg-blue-100' : showAccordion == 6}" @click="showAccordion = 6">
                            <a href="#" class="text-xs">Domestic glass</a>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-3 h-3">
                                <path fill-rule="evenodd" d="M12.53 16.28a.75.75 0 01-1.06 0l-7.5-7.5a.75.75 0 011.06-1.06L12 14.69l6.97-6.97a.75.75 0 111.06 1.06l-7.5 7.5z" clip-rule="evenodd" />
                            </svg>
                        </div>
                        <div class="bg-blue-100 p-5" x-show="showAccordion == 6">
                            <img class="w-40 h-28" src="images/2.jpg">
                        </div>
                    </li>
                </ul>
            </div>
            <div class="flex items-center justify-center p-6 pt-14">
                <div class="bg-white rounded-full shadow shadow-blue-500 p-2 shadow">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="w-6 text-blue-500 h-6">
                        <path fill-rule="evenodd" d="M12 3.75a.75.75 0 01.75.75v6.75h6.75a.75.75 0 010 1.5h-6.75v6.75a.75.75 0 01-1.5 0v-6.75H4.5a.75.75 0 010-1.5h6.75V4.5a.75.75 0 01.75-.75z" clip-rule="evenodd" />
                    </svg>
                </div>
            </div>
        </div>
    </div>
</div>