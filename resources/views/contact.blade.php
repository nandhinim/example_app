@extends('layouts.app')
@section('content')
@include('.contact._background')
<!-- <div class="container mx-auto p-20"> -->
<div class="lg:flex md:flex-card bg-gray-200 flex-card p-10">
    @include('.contact._input')
    @include('.contact._map')
</div>
<!-- </div> -->
@endsection