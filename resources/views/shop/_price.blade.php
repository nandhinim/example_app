<div class="lg:w-4/12 md:w-full w-full ">
    <div class="">
        <div class="pt-20">
            <p>FILTER BY PRICE</p>
        </div>
        <div class="range-slider pt-10">
            <div class="flex itmes-center justify-center">
                <p class=" text-sm mt-8">Price:</p>
                <span class="rangeValues mt-10" style="font-size:14px;position:none"></span>
                <a href="#" class="bg-teal-500 mx-4 text-white rounded mt-6 py-2 px-6 text-center text-xs ">FILTER</a>
            </div>
            <input class="text-sm w-full" value="0" min="2" max="35" step="2" type="range" style="top:37px;">
            <input value="0" min="10" max="35" step="2" type="range" style="top:37px;">
        </div>
    </div>
    <div class="pt-10 lg:w-4/12 md:w-full w-full">
        <div class="">
            <p>PRODUCT CATEGORIES</p>
        </div>
        <div class="pt-5">
            <ul>
                <li class="flex items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-3 h-3">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                    </svg>
                    <p class="mx-3">Clothing</p>
                </li>
                <li class="items-center flex mx-2 pt-2">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-3 h-3">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                    </svg>
                    <p class="mx-3 text-xs">Hoodies</p>
                </li>
                <li class="flex items-center mx-2 pt-4 pt-2">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-3 h-3">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                    </svg>
                    <p class="mx-3 text-xs">T-shirts</p>
                </li>
                <li class="flex items-center pt-2">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-3 h-3">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                    </svg>
                    <p class="mx-3">Music</p>
                </li>
                <li class="flex items-center mx-2 pt-4 pt-2">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-3 h-3">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                    </svg>
                    <p class="mx-3 text-xs">Albums</p>
                </li>
                <li class="flex items-center mx-2 pt-4 pt-2">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-3 h-3">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                    </svg>
                    <p class="mx-3 text-xs">Singles</p>
                </li>
                <li class="flex items-center mx-2 pt-4 pt-2">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-3 h-3">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                    </svg>
                    <p class="mx-3 text-xs">Posters</p>
                </li>
            </ul>
        </div>
    </div>
    <div class="pt-10">
        <div class="">
            <p>LATEST PRODUCTS</p>
        </div>
        <div class="">
            <ul>
                <li>
                    <div class="flex mt-5 items-center ">
                        <div class="ring-2 ring-gray-300 p-2">
                            <img class="w-10 h-10" src="images/woo1.jpg">
                        </div>
                        <div class="mx-4">
                            <p>Woo Single #2</p>
                            <div class="flex text-xs mt-1 font-bold">
                                <p class="line-through">{3.00</p>
                                <p class="mx-2 text-red-600">{2.00</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="flex mt-5 items-center ">
                        <div class="ring-2 ring-gray-300 p-2">
                            <img class="w-10 h-10" src="images/woo2.jpg">
                        </div>
                        <div class="mx-4">
                            <p>Woo Album #4</p>
                            <div class="flex text-xs mt-1 font-bold">
                                <!-- <p class="line-through">{3.00</p> -->
                                <p class="mx-2 text-gray-600">{9.00</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="flex mt-5 items-center ">
                        <div class="ring-2 ring-gray-300 p-2">
                            <img class="w-10 h-10" src="images/woo3.jpg">
                        </div>
                        <div class="mx-4">
                            <p>Woo Single #1</p>
                            <div class="flex text-xs mt-1 font-bold">
                                <!-- <p class="line-through">{3.00</p> -->
                                <p class="mx-2 text-gray-600">{9.00</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="flex mt-5 items-center ">
                        <div class="ring-2 ring-gray-300 p-2">
                            <img class="w-10 h-10" src="images/woo4.jpg">
                        </div>
                        <div class="mx-4">
                            <p>Woo Album #3</p>
                            <div class="flex text-xs mt-1 font-bold">
                                <!-- <p class="line-through">{3.00</p> -->
                                <p class="mx-2 text-gray-600">{9.00</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="flex mt-5 items-center ">
                        <div class="ring-2 ring-gray-300 p-2">
                            <img class="w-10 h-10" src="images/woo5.jpg">
                        </div>
                        <div class="mx-4">
                            <p>Woo Album #2</p>
                            <div class="flex text-xs mt-1 font-bold">
                                <!-- <p class="line-through">{3.00</p> -->
                                <p class="mx-2 text-gray-600">{9.00</p>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="pt-10">
        <div class="">
            <p>PRODUCTS ON SALE</p>
        </div>
        <div class="">
            <ul>
                <li>
                    <div class="flex mt-5 items-center ">
                        <div class="ring-2 ring-gray-300 p-2">
                            <img class="w-10 h-10" src="images/woo3.jpg">
                        </div>
                        <div class="mx-4">
                            <p>Woo Album #2</p>
                            <div class="flex text-xs mt-1 font-bold">
                                <!-- <p class="line-through">{3.00</p> -->
                                <p class="mx-2 text-gray-600">{9.00</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="flex mt-5 items-center ">
                        <div class="ring-2 ring-gray-300 p-2">
                            <img class="w-10 h-10" src="images/fly1.png">
                        </div>
                        <div class="mx-4">
                            <p>Woo Album #2</p>
                            <div class="flex text-xs mt-1 font-bold">
                                <!-- <p class="line-through">{3.00</p> -->
                                <p class="mx-2 text-gray-600">{9.00</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="flex mt-5 items-center ">
                        <div class="ring-2 ring-gray-300 p-2">
                            <img class="w-10 h-10" src="images/woo5.jpg">
                        </div>
                        <div class="mx-4">
                            <p>Woo Album #2</p>
                            <div class="flex text-xs mt-1 font-bold">
                                <!-- <p class="line-through">{3.00</p> -->
                                <p class="mx-2 text-gray-600">{9.00</p>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>