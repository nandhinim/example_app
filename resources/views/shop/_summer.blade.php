<div class="">
  <div class="slides">
    <div class="mySlides fade">
      <div class="numbertext">
        <img class="relative" src="images/back.webp" style="width:100%;height:100%">
        <div class="text absolute top-48">
          <p class="text-6xl">SHOP</p>
        </div>
      </div>
    </div>

    <div class="mySlides fade">
      <div class="numbertext"></div>
      <img src="images/3.jpg" class="relative" style="width:100%;height:100%">
      <div class="text absolute">
        <p class="text-6xl">SHOP</p>
      </div>
    </div>

    <div class="mySlides fade">
      <div class="numbertext"></div>
      <img src="images/5.jpg" class="relative" style="width:100%;height:100%">
      <div class="text absolute">
        <p class="text-6xl">SHOP</p>
      </div>
    </div>

    <a class="prev lg:top-80 md:top-60 top-60" onclick="plusSlides(-1)">❮</a>
    <a class="next lg:top-80 md:top-60 top-60" onclick="plusSlides(1)">❯</a>

  </div>
  <br>

  <div style="text-align:center">
    <span class="dot bg-red-500" onclick="currentSlide(1)"></span>
    <span class="dot" onclick="currentSlide(2)"></span>
    <span class="dot" onclick="currentSlide(3)"></span>
  </div>
</div>
<script>
  let slideIndex = 1;
  showSlides(slideIndex);

  function plusSlides(n) {
    showSlides(slideIndex += n);
  }

  function currentSlide(n) {
    showSlides(slideIndex = n);
  }

  function showSlides(n) {
    let i;
    let slides = document.getElementsByClassName("mySlides");
    let dots = document.getElementsByClassName("dot");
    if (n > slides.length) {
      slideIndex = 1
    }
    if (n < 1) {
      slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";
  }
</script>
<style>
  .dot {
    cursor: pointer;
    height: 15px;
    width: 15px;
    margin: 0 2px;
    background-color: #bbb;
    border-radius: 50%;
    display: inline-block;
    transition: background-color 0.6s ease;
    top: -80px;
  }
</style>