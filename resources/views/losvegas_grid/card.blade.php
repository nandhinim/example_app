@php
$users=collect((object)[
(object) [
'image'=>'images/1.jpg',
'country'=>'Las vegas',
'information'=>'4 Days 4 Nights - Stay in Las Vegas',
'offer'=>'$690',
'cost'=>'$567',
],
(object) [
'image'=>'images/2.jpg',
'country'=>'Bali Kuta,indonesia',
'information'=>'4 Days 4 Nights - For two at sol',
'offer'=>'$4300',
'cost'=>'$3092',
],
(object) [
'image'=>'images/3.jpg',
'country'=>'Sydney Australia',
'information'=>'4 Days 4 Nights - For to at airport',
'offer'=>'$1000',
'cost'=>'$922',
],
(object) [
'image'=>'images/5.jpg',
'country'=>'Las vegas',
'information'=>'4 Days 4 Nights - Stay in Las Vegas',
'offer'=>'$4300',
'cost'=>'$3092',
]


]);



@endphp
<div class="pt-10">
    <div class="">

        <div class="grid grid-cols-4 gap-4">
            @foreach($users as $key=>$user)
            <div class="">

                <div class="relative">

                    <img class=" rounded-xl w-full h-40" src="{{$user->image}}">
                    <div class="absolute bg-teal-500 w-full text-center text-sm font-semibold text-white py-2 rounded-xl bottom-0 left-0">
                        <a class="text-sm text-center" href="">Reserve Now</a>
                    </div>
                    <div class="absolute text-white backdrop-brightness-50 w-full rounded-xl bg-black/30 flex items-center justify-between top-0">

                        <div class="mx-5">
                            <p class="text-sm">{{$user->country}}</p>
                            <p class="text-xs" style="font-size:8px;">{{$user->information}}</p>
                        </div>
                        <div class="bg-blue-500 text-center rounded-xl ml-9 px-2 py-4">
                            <p class="text-xs">{{$user->offer}}</p>
                            <p class="text-sm mt-1" style="font-size:10px;">{{$user->cost}}</p>
                        </div>

                    </div>
                </div>

            </div>
            @endforeach
        </div>

    </div>
</div>