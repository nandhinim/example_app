<div class="subject-info-box-1 mt-4 mx-auto p-10" style="min-width:60%; min-height:45%;">
    <select multiple="multiple" id='lstBox1' class="form-control rounded border p-5" style="min-width:60%;min-height:45%;">
        <option class="p-2 rounded mt-2" value="ajax">Ajax</option>
        <option class="p-2 rounded mt-2" value="jquery">jQuery</option>
        <option class="p-2 rounded mt-2" value="javascript">JavaScript</option>
        <option class="p-2 rounded mt-2" value="mootool">MooTools</option>
        <option class="p-2 rounded mt-2" value="prototype">Prototype</option>
        <option class="p-2 rounded mt-2" value="dojo">Dojo</option>
    </select>
</div>

<div class="subject-info-arrows text-center mt-4 w-4/12 p-10 mx-auto">
    <input type='button' id='btnAllRight' value='>>' class=" border rounded px-4 mt-2 py-1" /><br />
    <input type='button' id='btnRight' value='>' class="border rounded px-5 py-1 mt-2" /><br />
    <input type='button' id='btnLeft' value='<' class="border rounded px-5 py-1 mt-2" /><br />
    <input type='button' id='btnAllLeft' value='<<' class="border rounded px-4 py-1 mt-2" />
</div>

<div class="subject-info-box-2 mt-4 p-10 mx-auto " style="min-width:60%;min-height:45%;">
    <select multiple="multiple" id='lstBox2' class="form-control rounded border p-5" style="min-width:60%;min-height:45%;">
        <option class="p-2 rounded mt-2" value="asp">ASP.NET</option>
        <option class="p-2 rounded mt-2" value="c#">C#</option>
        <option class="p-2 rounded mt-2" value="vb">VB.NET</option>
        <option class="p-2 rounded mt-2" value="java">Java</option>
        <option class="p-2 rounded mt-2" value="php">PHP</option>
        <option class="p-2 rounded mt-2" value="python">Python</option>
    </select>
</div>

<div class="clearfix"></div>
<style>
    /* .subject-info-box-1,
    .subject-info-box-2 {
        float: left;
        width: 45%;
    }

    .subject-info-box-1 select,
    .subject-info-box-2 select {
        height: 200px;
        padding: 0;
    }

    .subject-info-box-1 select option,
    .subject-info-box-2 select option {
        padding: 4px 10px 4px 10px;
    } */

    .subject-info-box-1 select option:hover,
    .subject-info-box-2 select option:hover {
        background: #EEEEEE;
        padding: 4px 10px 4px 10px;

    }

    /* .subject-info-arrows {
        float: left;
        width: 45%;
    }

    .subject-info-arrows input {
        width: 45%;
        margin-bottom: 5px;
    } */
</style>
<script>
    /*
     * Original example found here: http://www.jquerybyexample.net/2012/05/how-to-move-items-between-listbox-using.html
     * Modified by Esau Silva to support 'Move ALL items to left/right' and add better stylingon on Jan 28, 2016.
     * 
     */
    function strDes(a, b) {
        if (a.value > b.value) return 1;
        else if (a.value < b.value) return -1;
        else return 0;
    }

    console.clear();
    (function() {
        $('#btnRight').click(function(e) {
            var selectedOpts = $('#lstBox1 option:selected');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }
            $('#lstBox2').append($(selectedOpts).clone());
            $(selectedOpts).remove();
            e.preventDefault();
        });

        $('#btnAllRight').click(function(e) {
            var selectedOpts = $('#lstBox1 option');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }

            $('#lstBox2').append($(selectedOpts).clone());
            $(selectedOpts).remove();
            e.preventDefault();
        });

        $('#btnLeft').click(function(e) {
            var selectedOpts = $('#lstBox2 option:selected');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }

            $('#lstBox1').append($(selectedOpts).clone());
            $(selectedOpts).remove();
            e.preventDefault();
        });

        $('#btnAllLeft').click(function(e) {
            var selectedOpts = $('#lstBox2 option');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }

            $('#lstBox1').append($(selectedOpts).clone());
            $(selectedOpts).remove();
            e.preventDefault();
        });
    }(jQuery));
</script>