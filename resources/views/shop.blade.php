@extends('layouts.app')
@section('content')
@include('.shop._summer')
<div class="">
    <div class="container mx-auto ">
        <div class="lg:flex-row flex md:flex-col flex-col p-10">
            @include('.shop._tshirt')
            @include('.shop._price')
        </div>
    </div>
</div>
@endsection