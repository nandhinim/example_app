<div class="bg-gray-100">
    <div class="flex justify-between p-10 lg:w-8/12 md:w-full w-full mx-auto">
        <div class="text-xl">
            <p>HAPPY NINJA</p>
        </div>
        <div class="">
            <ul class="flex text-gray-500">
                <li class="flex justify-center items-center">
                    <p class="text-xs">Home</p>
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-2 mx-2 h-2">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                    </svg>
                </li>
                <li class="flex justify-center items-center mx-1">
                    <p class="text-xs">Shop</p>
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-2 mx-2 h-2">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                    </svg>
                </li>
                <li class="flex justify-center items-center mx-1">
                    <p class="text-xs">Clothing</p>
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-2 mx-2 h-2">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                    </svg>
                </li>
                <li class="flex justify-center items-center mx-1">
                    <p class="text-xs">Hoodies</p>
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-2 mx-2 h-2">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                    </svg>
                </li>
                <li>
                    <p class="text-xs">Happy Ninja</p>
                </li>
            </ul>
        </div>
    </div>
</div>