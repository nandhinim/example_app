<header>
    <div class="bg-gray-100 shadow flex items-center">
        <div class="flex justify-between items-center w-5/12 p-4">
            <div class="flex w-3/12 items-center justify-between">
                <img class="w-10 h-10" src="a1.png">
                <img class="w-4 h-4" src="menu.png">
            </div>
            <div class="flex items-center ">
                <p class="text-xs">Mega menu</p>
                <img class="w-3 h-3" src="downward-arrow.png">
            </div>
            <div class="flex relative">
                <input class="rounded-2xl p-2 text-center" type="text" name="name" placeholder="search">
                <img class="w-4 h-4 right-0 top-0 mt-3 mx-2  absolute" src="search-interface-symbol.png">
            </div>
        </div>
        <div class=" w-6/12" x-data="{showAccordion:0}">
            <div class="">
                <div class="float-right flex items-center justify-between">
                    <!-- <div class="pr-3">
                        <img class="w-4 h-4" src="search-interface-symbol.png">
                    </div>
                    <div class="pr-3">
                        <img class="w-4 h-4" src="direction.png">
                    </div>
                    <div class="pr-3">
                        <img class="w-4 h-4" src="settings.png">
                    </div>
                    <div class="pr-3">
                        <img class="w-4 h-4" src="notifications.png">
                    </div> -->
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z" />
                        </svg>
                    </div>
                    <div class="">
                        <div class="mx-4 relative">
                            <a href="#" @click="showAccordion = 2"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M14.857 17.082a23.848 23.848 0 005.454-1.31A8.967 8.967 0 0118 9.75v-.7V9A6 6 0 006 9v.75a8.967 8.967 0 01-2.312 6.022c1.733.64 3.56 1.085 5.455 1.31m5.714 0a24.255 24.255 0 01-5.714 0m5.714 0a3 3 0 11-5.714 0M3.124 7.5A8.969 8.969 0 015.292 3m13.416 0a8.969 8.969 0 012.168 4.5" />
                                </svg></a>
                        </div>
                        <div class="absolute top-28 h-full -right-10 bg-gray-200 rounded p-4 z-20" x-transition:enter="transition ease-out duration-500" x-transition:enter-start="opacity-0 translate-x-6" x-transition:enter-end="opacity-100 translate-x-6" x-transition:leave="transition ease-linear duration-300" x-transition:leave-start="opacity-100 translate-x-6" x-transition:leave-end="opacity-0 translate-x-6" x-show="showAccordion == 2">
                            <div class="flex items-center rounded-xl mx-4 bg-gray-100 shadow py-2 p-4">
                                <div class="mx-2">
                                    <img class="w-12 h-12 rounded-full" src="images/aisy.jpg">
                                </div>
                                <div class="mx-2">
                                    <p class="font-semibold">Aisy</p>
                                    <p class="text-xs mt-1">aisy@gmail.com</p>
                                </div>
                                <div class="">
                                    <a href="#" @click="showAccordion = -1"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
                                        </svg></a>
                                </div>
                            </div>
                            <div class="flex items-center rounded-xl mt-4 mx-4 bg-gray-100 shadow py-2 p-4">
                                <div class="mx-2">
                                    <img class="w-12 h-12 rounded-full" src="images/aisy.jpg">
                                </div>
                                <div class="mx-2">
                                    <p class="font-semibold">Aisy</p>
                                    <p class="text-xs mt-1">aisy@gmail.com</p>
                                </div>
                                <div class="">
                                    <a href="#" @click="showAccordion = -1"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
                                        </svg></a>
                                </div>
                            </div>
                            <div class="flex items-center rounded-xl mt-4 mx-4 bg-gray-100 shadow py-2 p-4">
                                <div class="mx-2">
                                    <img class="w-12 h-12 rounded-full" src="images/aisy.jpg">
                                </div>
                                <div class="mx-2">
                                    <p class="font-semibold">Aisy</p>
                                    <p class="text-xs mt-1">aisy@gmail.com</p>
                                </div>
                                <div class="">
                                    <a href="#" @click="showAccordion = -1"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
                                        </svg></a>
                                </div>
                            </div>
                            <div class="flex items-center rounded-xl mt-4 mx-4 bg-gray-100 shadow py-2 p-4">
                                <div class="mx-2">
                                    <img class="w-12 h-12 rounded-full" src="images/aisy.jpg">
                                </div>
                                <div class="mx-2">
                                    <p class="font-semibold">Aisy</p>
                                    <p class="text-xs mt-1">aisy@gmail.com</p>
                                </div>
                                <div class="">
                                    <a href="#" @click="showAccordion = -1"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
                                        </svg></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="flex items-center relative mx-4 bg-gray-100 rounded-xl shadow py-2 p-4">
                            <div class="mx-2" @click="showAccordion = 1">
                                <img class="w-12 h-12 rounded-full" src="images/aisy.jpg">
                            </div>
                            <div class="mx-2">
                                <p class="font-semibold">Aisy</p>
                                <p class="text-xs mt-1">aisy@gmail.com</p>
                            </div>
                        </div>
                        <div id="side" class="bg-gray-100 backdrop-sepia-0 top-34 -right-0  bg-gray/40 z-10 shadow absolute h-full text-xs w-78 pt-10" x-transition:enter="transition ease-linear duration-500" x-transition:enter-start="opacity-0 translate-x-6" x-transition:enter-end="opacity-100 translate-x-6" x-transition:leave="transition ease-linear duration-300" x-transition:leave-start="opacity-100 translate-x-6" x-transition:leave-end="opacity-0 translate-x-6" x-show="showAccordion == 1">
                            <div class="flex items-center relative mx-4 rounded-xl bg-gray-100 shadow py-2 p-4">
                                <div class="mx-2">
                                    <img class="w-12 h-12 rounded-full" src="images/aisy.jpg">
                                </div>
                                <div class="mx-2">
                                    <p class="font-semibold">Aisy</p>
                                    <p class="text-xs mt-1">aisy@gmail.com</p>
                                </div>
                                <div class="">
                                    <a href="#" @click="showAccordion = -1"><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
                                        </svg></a>
                                </div>
                            </div>
                            <div class="font-semibold text-gray-600 text-center mt-5">
                                <p class="mt-5">Edit Profile Details</p>
                                <p class="mt-5">Edit Status</p>
                                <p class="mt-5">Notification</p>
                                <p class="mt-5">Help & Support
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<style>

</style>