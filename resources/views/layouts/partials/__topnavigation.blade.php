<header>
  <div class="header-wrapper custom-bg arrow-bottom shadow">
    <div class="px-3 p-10  shadow">
      <div class="container mx-auto ">
        <div class="flex lg:h-12 justify-between relative flex-col lg:flex-row lg:items-center">
          <div class="w-full lg:w-1/4 flex justify-between">
            <div class="w-40">
              <a href="{{url('home')}}"><img class="w-20 h-20" src="images/h.png" class="h-auto"></a>
            </div>
            <div class="block lg:hidden menu-click">
              <button class="flex items-center px-3 py-2 border rounded text-teal-lighter border-teal-light ">
                <svg class="fill-current h-3 w-3 " viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                  <title>Menu</title>
                  <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
                </svg>
              </button>
            </div>
          </div>
          <div class="w-full menu-open hidden md:absolute lg:static absolute lg:z-0 md:z-20 z-20 lg:bg-none md:bg-white bg-gray-500 lg:p-0 md:p-4 p-10 lg:block md:hidden" style="top: 80px;width:65%;">
            <div class=" flex flex-col lg:flex-row md:flex-row relative  justify-end ">
              <ul class="list-reset  flex flex-col lg:flex-row leading-loose lg:justify-center md:justify-none justify-none text-sm tracking-wider py-1">
                <li class="mx-3 flex items-center ho1 rlative lg:justify-center md:justify-none justify-none">
                  <a href="#" class="no-underline uppercase cursor-pointer">PAGES</a>
                  <svg xmlns="http://www.w3.org/2000/svg" class="h-3 w-3 mx-2" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M19 9l-7 7-7-7" />
                  </svg>
                  <div class="first-page absolute w-3/12">
                    <ul class="bg-black p-2 text-center border-t-2 border-teal-500 text-white">
                      <li class=""><a href="#">Home Pages</a></li>
                      <li class="pt-3"><a href="#">New Pages</a></li>
                      <li class="pt-3"><a href="#">About us</a></li>
                      <li class="pt-3"><a href="#">Services</a></li>
                      <li class="pt-3"><a href="#">Contact Pages</a></li>
                      <li class="pt-3"><a href="#">Shop Pages</a></li>
                    </ul>
                  </div>
                </li>
                <li class="mx-3 flex items-center ho2 relative lg:justify-center md:justify-none justify-none">
                  <a href="#" class="no-underline uppercase  cursor-pointer">FEATURES</a>
                  <svg xmlns="http://www.w3.org/2000/svg" class="h-3 w-3 mx-2" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M19 9l-7 7-7-7" />
                  </svg>
                  <div class="second-page absolute">
                    <ul class="bg-black p-2 text-center border-t-2 border-teal-500 text-white">
                      <li class=""><a href="#">Features Overview</a></li>
                      <li class="pt-3"><a href="#">Tons of Elements</a></li>
                      <li class="pt-3"><a href="#">Responsive Design</a></li>
                      <li class="pt-3"><a href="#">Visual Composer</a></li>
                      <li class="pt-3"><a href="#">Advanced Admin Options</a></li>
                      <li class="pt-3"><a href="#">Unlimited Colors</a></li>
                    </ul>
                  </div>
                </li>
                <li class="mx-3 flex items-center lg:justify-center md:justify-none justify-none">
                  <a href="#" class="no-underline uppercase  cursor-pointer">PORTFOLIO</a>
                  <svg xmlns="http://www.w3.org/2000/svg" class="h-3 w-3 mx-2" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M19 9l-7 7-7-7" />
                  </svg>
                </li>
                <li class="mx-3 flex items-center lg:justify-center md:justify-none justify-nonejustify-center">
                  <a href="{{url('blog')}}" class="no-underline uppercase  cursor-pointer">BLOG</a>
                  <svg xmlns="http://www.w3.org/2000/svg" class="h-3  w-3 mx-2" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M19 9l-7 7-7-7" />
                  </svg>
                </li>
                <li class="mx-3 "><a href="{{url('shop')}}" class="no-underline uppercase  cursor-pointer">SHOP</a></li>
                <li class="mx-3"><a href="{{url('contact')}}" class="no-underline uppercase  cursor-pointer">CONTACT</a></li>
                <li class="mx-3"><a href="#" class="no-underline border px-4 py-2 uppercase  cursor-pointer">BUY THEME</a></li>
              </ul>
              <!-- <div class="first_page">
          <ul>
            <li>
              <a href="#">Home Pages</a>
              <svg xmlns="http://www.w3.org/2000/svg" class="h-3 w-3 mx-2" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
               <path stroke-linecap="round" stroke-linejoin="round" d="M19 9l-7 7-7-7" />
                 </svg>
            </li>
            <li>
              <a href="#">Home Pages</a>
              <svg xmlns="http://www.w3.org/2000/svg" class="h-3 w-3 mx-2" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
               <path stroke-linecap="round" stroke-linejoin="round" d="M19 9l-7 7-7-7" />
                 </svg>
            </li>
            <li>
              <a href="#">Home Pages</a>
              <svg xmlns="http://www.w3.org/2000/svg" class="h-3 w-3 mx-2" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
               <path stroke-linecap="round" stroke-linejoin="round" d="M19 9l-7 7-7-7" />
                 </svg>
            </li>
            <li>
              <a href="#">Home Pages</a>
              <svg xmlns="http://www.w3.org/2000/svg" class="h-3 w-3 mx-2" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
               <path stroke-linecap="round" stroke-linejoin="round" d="M19 9l-7 7-7-7" />
                 </svg>
            </li>
</ul>
        </div> -->
            </div>

          </div>
        </div>
      </div>
    </div>

  </div>

</header>