<footer>
    <div class="p-10 bg-teal-500">
        <div class=" container  text-white">
            <div class="lg:flex-row flex md:flex-col flex-col items-center justify-center justify-between ">
                <p class="lg:text-xl md:text-sm text-sm">Let's work on your exciting new project together! Join our long,
                    happy-client list now.
                </p>
                <div class=" py-3 px-6 w-fit rounded-full border-2 text-center lg:text-sm md:text-xs text-xs border-white">
                    <a href="#" class="">BUY NOW ></a>
                </div>
            </div>
        </div>
    </div>
    <div class="">
        <div class="bg-gray-800">
            <div class="grid grid-cols-1 lg:grid-cols-4 md:grid-cols-2 gap-4 text-gray-400 lg:p-20 md:p-5 p-5">
                <div class="">
                    <div class="">
                        <img class="w-40 h-10" src="images/c.png">
                    </div>
                    <div class="pt-10 text-sm">
                        <p>Fortuna is a modern WordPress Theme that features clean responsive design and is loaded with
                            plenty of hot features. You and your clients will fall in love with its slick vision and
                            full spec package.</p>
                    </div>
                </div>
                <div class="">
                    <div class="">
                        <p>FOOTER MENU</p>
                    </div>
                    <div class="pt-10">
                        <ul class="text-xs">
                            <li class="flex pt-2">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                                </svg>
                                <p>Home Page</p>
                            </li>
                            <li class="flex pt-2">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                                </svg>
                                <p>About the Company</p>
                            </li>
                            <li class="flex pt-2">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                                </svg>
                                <p>Our great Services</p>
                            </li>
                            <li class="flex pt-2">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                                </svg>
                                <p>Check our Portfolio</p>
                            </li>
                            <li class="flex pt-2">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                                </svg>
                                <p>Read Our Blog</p>
                            </li>
                            <li class="flex pt-2">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                                </svg>
                                <p>Contact us now</p>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="">
                    <div class="">
                        <p class="">LATEST NEWS</p>
                    </div>
                    <div class="flex pt-10 items-center">
                        <div class="">
                            <img class="w-20 h-20" src="images/1.jpg">
                        </div>
                        <div class="mx-2 text-xs">
                            <p>LATEST NEWS POST</p>
                            <i>April 23,2015</i>
                        </div>
                    </div>
                    <div class="flex pt-5 items-center">
                        <div class="">
                            <img class="w-20 h-20" src="images/1.jpg">
                        </div>
                        <div class="mx-2 text-xs">
                            <p>LATEST NEWS POST</p>
                            <i>April 23,2015</i>
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="">
                        <p>CONTACT INFO</p>
                    </div>
                    <div class="pt-10">
                        <ul class="text-sm">
                            <li class="flex pt-2">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 18h.01M8 21h8a2 2 0 002-2V5a2 2 0 00-2-2H8a2 2 0 00-2 2v14a2 2 0 002 2z" />
                                </svg>
                                <p class="mx-4">+ 357 720 1443</p>
                            </li>
                            <li class="flex pt-4">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                                </svg>
                                <p class="mx-4">contact@company.com</p>
                            </li>
                            <li class="flex pt-4">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                    <path fill-rule="evenodd" d="M5.05 4.05a7 7 0 119.9 9.9L10 18.9l-4.95-4.95a7 7 0 010-9.9zM10 11a2 2 0 100-4 2 2 0 000 4z" clip-rule="evenodd" />
                                </svg>
                                <p class="mx-4">362 52nd Street, New York City, NY</p>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</footer>