@extends('layouts.myaccount')
@section('content')

                <div class="flex items-center">
                    <div class="">
                        <p class="text-lg">Version 1</p>
                    </div>
                    <div class="pl-4 ">
                        <p class="text-xs flex items-center ">Dashboard <span class="px-2">|</span>Version 1
                        </p>
                    </div>
                </div>
                <div class="pt-6">
                    <hr>
                </div>
                <div class="grid grid-cols-1 lg:grid-cols-4 md:grid-cols-2 gap-4 pt-10">
                    <div class="bg-white shadow rounded p-4 items-center justify-between flex">
                        <img class="w-8 h-8" src="mathematics-tool.png">
                        <div class="text-center">
                            <p class="text-xs">New Leads</p>
                            <p>205</p>
                        </div>
                    </div>
                    <div class="bg-white shadow rounded p-4 items-center justify-between flex">
                        <img class="w-8 h-8" src="save-money.png">
                        <div class="text-center">
                            <p class="text-xs">Sales</p>
                            <p>4021</p>
                        </div>
                    </div>
                    <div class="bg-white shadow rounded p-4 items-center justify-between flex">
                        <img class="w-8 h-8" src="paint-bucket.png">
                        <div class="text-center">
                            <p class="text-xs">Orders</p>
                            <p>80</p>
                        </div>
                    </div>
                    <div class="bg-white shadow rounded p-4 items-center justify-between flex">
                        <img class="w-8 h-8" src="dollar.png">
                        <div class="text-center">
                            <p class="text-xs">Expense</p>
                            <p>120</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   

@endsection