  <!-- header start -->
  <header>
      <div class="custom-header py-5" x-data="{ menuOpen: false }">
          <div class="container mx-auto">
              <div class="flex justify-between">
                  <div>
                      <!-- <img src="images/logo.png" class="w-16"> -->
                  </div>

                  <div>
                      <ul class="flex text-white">
                          <li class="px-4 relative">
                              <!-- <a href="#" @click="menuOpen = true">Home</a> onclick="show()"-->
                              <a href="#" @click="menuOpen = true">Home</a>
                              <ul class="bg-white w-40 absolute shadow p-3 text-black show-menu " x-show="menuOpen" @click.away="menuOpen = false">
                                  <!-- x-show="menuOpen" @click.away="menuOpen = false" -->
                                  <li>Menu 1</li>
                                  <li>Menu 2</li>
                                  <li>Menu 3</li>
                              </ul>
                          </li>
                          <li class="px-4"><a href="#">About us</a></li>
                          <li class="px-4"><a href="#">Contact</a></li>
                          <li class="px-4"><a href="#">Help</a></li>
                          <li class="px-4"><a href="#">Login</a></li>
                          <li class="px-4"><a href="#">Register</a></li>
                      </ul>
                  </div>
              </div>
          </div>
      </div>
  </header>
  <!-- header end -->
  <!-- main section -->
  <!--  <div>
        <table>
            <thead>
                <th>1</th>
                <th>2</th>
                <th>3</th>
            </thead>
            <tbody>
            
                <tr>
                    <td>11</td>
                    <td>22</td>
                    <td>33</td>
                </tr>
                
            
            </tbody>
        </table> -->

  <!-- tab section start -->
  <div x-data="{tabOpen: 'tabOne'}" class="shadow w-1/2 mx-auto my-5">
      <ul class="flex border-b">
          <li class="px-3 py-2" :class="{'border-b-2 border-green-500' : tabOpen == 'tabOne'}"><a href="#" @click="tabOpen = 'tabOne'">Tab 1</a></li>
          <li class="px-3 py-2" :class="{'border-b-2 border-green-500' : tabOpen == 'tabTwo'}"><a href="#" @click="tabOpen='tabTwo'">Tab 2</a></li>
          <li class="px-3 py-2" :class="{'border-b-2 border-green-500' : tabOpen == 'tabThree'}"><a href="#" @click="tabOpen='tabThree'">Tab 3</a></li>
      </ul>
      <div x-show="tabOpen == 'tabOne'" class="text-xs p-3">
          orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
      </div>
      <div x-show="tabOpen == 'tabTwo'" class="p-3">
          content 2
      </div>
      <div x-show="tabOpen == 'tabThree'" class="p-3">
          content 3
      </div>
  </div>
  <!-- tab section end -->

  <!-- accordion start -->
  <div x-data="{showAccordion:0}" class="w-1/2 mx-auto py-3">
      <div class="shadow my-3">
          <div @click="showAccordion = 1" class="bg-gray-100 p-3 flex justify-between">
              <p :class="{'font-bold' : showAccordion == 1}">Qustion 1</p>
              <!--    <div x-text="showAccordion == 1 ? '+' : '-'"></div> -->
              <div :class="{'transform rotate-180' : showAccordion == 1}">
                  <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                      <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                  </svg>
              </div>
          </div>
          <div x-show="showAccordion == 1" class="p-3 text-xs">
              orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
          </div>
      </div>
      <div class="shadow my-3">
          <div @click="showAccordion = 2" class="bg-gray-100 p-3 flex justify-between">
              <p :class="{'font-bold' : showAccordion == 2}">Qustion 2</p>
              <div :class="{'transform rotate-180' : showAccordion == 1}">
                  <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                  </svg>
              </div>
          </div>
          <div x-show="showAccordion == 2" class="p-3">
              Answer 2
          </div>
      </div>
      <div class="shadow my-3">
          <div @click="showAccordion = 3" class="bg-gray-100 p-3 flex justify-between">
              <p :class="{'font-bold' : showAccordion == 3}">Qustion 3</p>
              <div :class="{'transform rotate-180' : showAccordion == 1}">
                  <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                  </svg>
              </div>
          </div>
          <div x-show="showAccordion == 3" class="p-3">
              Answer 3
          </div>
      </div>

  </div>

  <!-- accordion end -->


  <!-- slider start -->
  <!-- <div class="bg-gray-100 p-5">
      <div class="w-4/5 mx-auto relative" x-data="{ activeSlide: 1, slides: [
      {id:1,image: 'b1.jpg'}, 
      {id:2,image: 'b2.jpg'},
      {id:3,image: 'b3.jpg'}, 
      {id:4,image: 'b1.jpg'},
      {id:5,image: 'b2.jpg'}
      ]}"> -->


  <!-- <div 
    class="max-w-4xl mx-auto relative" x-data="carouselData([
               { id: 1, text: 'First', image: 'logo.png' },
               { id: 2, text: 'Second', image: 'favicon.png' },
               { id: 3, text: 'Third', image: 'logo.png' },
               { id: 4, text: 'Forth', image: 'favicon.png' },
               { id: 5, text: 'Fifth', image: 'logo.png' }
       ])"
   > -->
  <!-- slides:[1,2,3,4] -->
  <!-- Slides -->
  <!-- <template x-for="slide in slides">
       :key="slide.id" -->
  <!-- <div x-show="activeSlide === slide.id" class="w-1/2 h-64 mx-auto">
          <img :src="`images/${slide.image}`" class="w-full h-full">
           <span class="text-teal-300">/</span> -->
  <!--   <span class="w-12 text-center" x-text="sliders.length"></span> -->
  <!-- </div>
  </template> -->

  <!-- Prev/Next Arrows -->
  <!-- <div class="absolute inset-0 flex">
      <div class="flex items-center justify-start w-1/2">
          <button class="bg-teal-100 text-teal-500 hover:text-orange-500 font-bold hover:shadow-lg rounded-full w-12 h-12 -ml-6" x-on:click="activeSlide = activeSlide === 1 ? slides.length : activeSlide - 1">
              &#8592;
          </button>
      </div>
      <div class="flex items-center justify-end w-1/2">
          <button class="bg-teal-100 text-teal-500 hover:text-orange-500 font-bold hover:shadow rounded-full w-12 h-12 -mr-6" x-on:click="activeSlide = activeSlide === slides.length ? 1 : activeSlide + 1">
              &#8594;
          </button>
      </div>
  </div>

  </div>

  </div>
  </div> -->
  <!-- slider end -->

  </div>

  <!-- <div x-data="{ numOrder: false }">
      <button x-on:click="numOrder = !numOrder" x-text="numOrder ? 'Remove your order number': 'Insert your order number'"></button>
      <input x-show="numOrder" type="text" placeholder="Order Number">
  </div> -->