<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<main>
    <!-- <h2>Beautiful Google Charts</h2>
    <h5>Daily Page Hits</h5> -->
    <div id="bar-chart"></div>
</main>
<style>
    @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);

    body {
        font-family: "Open Sans", Arial;
        background: #EDEDED;
    }

    main {
        width: 840px;
        margin: 10px auto;
        padding: 10px 20px 30px;
        background: #FFF;
        box-shadow: 0 3px 5px rgba(0, 0, 0, 0.2);
    }

    /* p {
        margin-top: 2rem;
        font-size: 13px;
    } */

    #bar-chart {
        width: 800px;
        height: 300px;
        position: relative;
    }

    #bar-chart::before {
        content: "";
        position: absolute;
        display: block;
        width: 240px;
        height: 30px;
        left: 155px;
        top: 254px;
        background: #FAFAFA;
        box-shadow: 1px 1px 0 0 #DDD;
    }
</style>
<script>
    google.load("visualization", "1", {
        packages: ["corechart"]
    });
    google.setOnLoadCallback(drawCharts);

    function drawCharts() {

        // BEGIN BAR CHART
        /*
        // create zero data so the bars will 'grow'
        var barZeroData = google.visualization.arrayToDataTable([
          ['Day', 'Page Views', 'Unique Views'],
          ['Sun',  0,      0],
          ['Mon',  0,      0],
          ['Tue',  0,      0],
          ['Wed',  0,      0],
          ['Thu',  0,      0],
          ['Fri',  0,      0],
          ['Sat',  0,      0]
        ]);
          */
        // actual bar chart data
        var barData = google.visualization.arrayToDataTable([
            ['Day', 'Page Views', 'Unique Views'],
            ['Sun', 1050, 600],
            ['Mon', 1370, 910],
            ['Tue', 660, 400],
            ['Wed', 1030, 540],
            ['Thu', 1000, 480],
            ['Fri', 1170, 960],
            ['Sat', 660, 320]
        ]);
        // set bar chart options
        var barOptions = {
            focusTarget: 'category',
            backgroundColor: 'transparent',
            colors: ['cornflowerblue', 'tomato'],
            fontName: 'Open Sans',
            chartArea: {
                left: 50,
                top: 10,
                width: '100%',
                height: '70%'
            },
            bar: {
                groupWidth: '80%'
            },
            hAxis: {
                textStyle: {
                    fontSize: 11
                }
            },
            vAxis: {
                minValue: 0,
                maxValue: 1500,
                baselineColor: '#DDD',
                gridlines: {
                    color: '#DDD',
                    count: 4
                },
                textStyle: {
                    fontSize: 11
                }
            },
            legend: {
                position: 'bottom',
                textStyle: {
                    fontSize: 12
                }
            },
            animation: {
                duration: 1200,
                easing: 'out',
                startup: true
            }
        };
        // draw bar chart twice so it animates
        var barChart = new google.visualization.ColumnChart(document.getElementById('bar-chart'));
        //barChart.draw(barZeroData, barOptions);
        barChart.draw(barData, barOptions);
    }
</script>