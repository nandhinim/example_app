<div id='calendar'></div>
<!-- <span><input type='color' id='colorPicker'></span> -->
<style>
    body {
        margin: 40px 10px;
        padding: 0;
        font-family: "Lucida Grande", Helvetica, Arial, Verdana, sans-serif;
        font-size: 14px;
    }

    #calendar {
        max-width: 900px;
        margin: 0 auto;
    }

    #wrap {
        width: 1100px;
        margin: 0 auto;
    }

    .closeBtn {
        border-radius: 5px;
    }

    .fc button {
        height: 3.1em;
    }

    .fc-month-button::before {
        margin-left: 0;
        content: "\f073";
        font-family: 'FontAwesome';
        color: gray;
        display: flex;
        /* top: -4px; */
        text-align: center;
        /* font-size: 11px; */
        /* padding-bottom: 45px; */
        justify-items: center;
        justify-content: center;
        border: none;

    }

    .fc-agendaWeek-button::before {
        content: "\f133";
        font-family: 'FontAwesome';
        color: gray;
        display: flex;
        /* top: -4px; */
        text-align: center;
        /* font-size: 11px; */
        /* padding-bottom: 45px; */
        justify-items: center;
        justify-content: center;
        border: none;
    }

    .fc-toolbar h2 {
        margin: 13px;
    }

    .fc-today-button {
        height: 2.1em;
        width: 83px;
    }

    .fc-content {
        display: flex;
        align-items: center;
    }

    .fc .fc-button-group>:first-child {
        margin-left: 0;
        border: none;
    }

    .fc .fc-button-group>* {
        float: left;
        margin: 0 0 0 -1px;
        border: none;
    }

    .fc-agendaDay-button::before {
        content: "\f271";
        font-family: 'FontAwesome';
        color: gray;
        display: flex;
        /* top: -4px; */
        text-align: center;
        /* font-size: 11px; */
        /* padding-bottom: 45px; */
        justify-items: center;
        justify-content: center;
    }

    .fc-button-group {
        border: none;
    }
</style>
<script>
    $(document).ready(function() {
        $("#calendar").fullCalendar({
            header: {
                left: "prev,next title",
                center: "month,agendaWeek,agendaDay",
                right: ""
            },
            defaultView: "month",
            navLinks: true, // can click day/week names to navigate views
            selectable: true,
            selectHelper: false,
            editable: true,
            eventLimit: true, // allow "more" link when too many events

            select: function(start, end) {
                var title = prompt("Event Content:");
                var eventData;
                if (title) {
                    eventData = {
                        title: title,
                        start: start,
                        end: end,
                        color: $("#colorPicker").val()
                    };
                    $("#calendar").fullCalendar("renderEvent", eventData, true); // stick? = true
                }
                $("#calendar").fullCalendar("unselect");
            },

            eventRender: function(event, element) {
                element
                    .find(".fc-content")
                    .prepend("<span class='closeBtn material-icons'>&#xe5cd;</span>");
                element.find(".closeBtn").on("click", function() {
                    $("#calendar").fullCalendar("removeEvents", event._id);
                });
            },

            eventClick: function(calEvent) {
                var title = prompt("Edit Event Content:", calEvent.title);
                calEvent.title = title;
                $("#calendar").fullCalendar("updateEvent", calEvent);

            },
        });
    });
</script>