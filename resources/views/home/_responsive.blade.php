<div class="container mx-auto pt-20">
        <div class="lg:flex md:flex-card flex-card">
            <div class="p-10">
                <div class="">
                    <div class="flex items-center justify-between ">
                        <p class="text-xl">RESPONSIVE DESIGN</p>
                        <div class="bg-teal-500  p-2 rounded-full">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6    text-white  w-6" fill="none"
                                viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                    d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z" />
                            </svg>
                        </div>
                    </div>
                    <div class="text-sm">
                        <p>Fortuna is a responsive, clean design wordpress theme that features plenty of cool
                            features</p>
                    </div>
                </div>
                <div class="pt-10">
                    <div class="flex items-center justify-between ">
                        <p class="text-xl">VISUAL BUILDER</p>
                        <div class="bg-teal-500  p-2 rounded-full">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 text-white w-6" fill="none"
                                viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                    d="M9.663 17h4.673M12 3v1m6.364 1.636l-.707.707M21 12h-1M4 12H3m3.343-5.657l-.707-.707m2.828 9.9a5 5 0 117.072 0l-.548.547A3.374 3.374 0 0014 18.469V19a2 2 0 11-4 0v-.531c0-.895-.356-1.754-.988-2.386l-.548-.547z" />
                            </svg>
                        </div>
                    </div>
                    <div class="text-sm">
                        <p>Fortuna features a highly customized version of Visual Composer – one of the best visual
                            builder</p>
                    </div>
                </div>
            </div>
            <div class="">
                <img src="images/iphone.png">
            </div>
            <div class="p-10">
                <div class="">
                    <div class="flex items-center justify-between ">
                        <p class="text-xl">POWERFUL BACKEND</p>
                        <div class="bg-teal-500  p-2 rounded-full">
                            <img class="w-6 h-6" src="images/die.png">
                        </div>
                    </div>
                    <div class="text-sm">
                        <p>Fortuna is a responsive, clean design wordpress theme that features plenty of cool feature
                        </p>
                    </div>
                </div>
                <div class="pt-10">
                    <div class="flex items-center justify-between ">
                        <p class="text-xl">RESPONSIVE DESIGN</p>
                        <div class="bg-teal-500  p-2 rounded-full">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 text-white w-6" fill="none"
                                viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                    d="M8 12h.01M12 12h.01M16 12h.01M21 12c0 4.418-4.03 8-9 8a9.863 9.863 0 01-4.255-.949L3 20l1.395-3.72C3.512 15.042 3 13.574 3 12c0-4.418 4.03-8 9-8s9 3.582 9 8z" />
                            </svg>
                        </div>
                    </div>
                    <div class="text-sm">
                        <p>Fortuna is a responsive, clean design wordpress theme that features plenty of cool
                            features</p>
                    </div>
                </div>
            </div>
        </div>
    </div>