<div class="w-11/12 mx-auto">
    <div class="grid grid-cols-1 lg:grid-cols-3 md:grid-cols-2 lg:gap-16 md:gap-5 gap-5 lg:p-16 md:p-5 p-5">
        <div class="text-center">
            <div class="">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-32 p-6 mx-auto rounded-full bg-teal-500 text-white w-32 " fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M9.663 17h4.673M12 3v1m6.364 1.636l-.707.707M21 12h-1M4 12H3m3.343-5.657l-.707-.707m2.828 9.9a5 5 0 117.072 0l-.548.547A3.374 3.374 0 0014 18.469V19a2 2 0 11-4 0v-.531c0-.895-.356-1.754-.988-2.386l-.548-.547z" />
                </svg>
                <p class="pt-5 font-semibold">STUNNING DESIGN</p>
                <div class="bg-teal-500 w-20 mx-auto rounded-full mt-4 h-1"></div>
            </div>
            <div class="pt-5">
                <p class="text-sm">Fortuna is a clean design, powerful WordPress Template suitable for a wide
                    variety of websites
                    that you and your clients will love</p>
            </div>
        </div>
        <div class="text-center">
            <div class="">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-32 mx-auto text-white bg-pink-700 p-6 rounded-full w-32" viewBox="0 0 20 20" fill="currentColor">
                    <path d="M11 17a1 1 0 001.447.894l4-2A1 1 0 0017 15V9.236a1 1 0 00-1.447-.894l-4 2a1 1 0 00-.553.894V17zM15.211 6.276a1 1 0 000-1.788l-4.764-2.382a1 1 0 00-.894 0L4.789 4.488a1 1 0 000 1.788l4.764 2.382a1 1 0 00.894 0l4.764-2.382zM4.447 8.342A1 1 0 003 9.236V15a1 1 0 00.553.894l4 2A1 1 0 009 17v-5.764a1 1 0 00-.553-.894l-4-2z" />
                </svg>
                <p class="pt-5 font-semibold">VISUAL BUILDER</p>
                <div class="bg-teal-500 rounded-full w-20 mx-auto mt-4 h-1"></div>
            </div>
            <div class="pt-5">
                <p class="text-sm">Content creation is a breeze with the drag’n drop Visual Composer and tons of
                    modification options available at the click of a button</p>
            </div>
        </div>
        <div class="text-center">
            <div class="">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-32 mx-auto text-white bg-pink-600 p-6 rounded-full w-32" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M15.536 8.464a5 5 0 010 7.072m2.828-9.9a9 9 0 010 12.728M5.586 15H4a1 1 0 01-1-1v-4a1 1 0 011-1h1.586l4.707-4.707C10.923 3.663 12 4.109 12 5v14c0 .891-1.077 1.337-1.707.707L5.586 15z" />
                </svg>
                <p class="pt-5 font-semibold">AMAZING SUPPORT</p>
                <div class="bg-teal-500 w-20 rounded-full mx-auto mt-4 h-1"></div>
            </div>
            <div class="pt-5">
                <p class="text-sm">With a 5 star client support we are available to respond to any issues or
                    questions you may have at our <span class="text-teal-500" style="top:0;font-size:10px;">Support
                        Forum </span>at any time</p>
            </div>
        </div>
        <div class="text-center">
            <div class="">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-32 mx-auto text-white bg-green-400 p-6 rounded-full w-32" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9" />
                </svg>
                <p class="pt-5 font-semibold">RICH BACKEND</p>
                <div class="bg-teal-500 w-20 rounded-full mx-auto mt-4 h-1"></div>
            </div>
            <div class="pt-5">
                <p class="text-sm">Fortuna ships with Visual Composer, Revolution Slider, 1200+ icons, google fonts,
                    unlimited colors, tons of shortcodes and theme options etc.</p>
            </div>
        </div>
        <div class="text-center">
            <div class="">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-32 mx-auto text-white bg-orange-500 p-6 rounded-full w-32" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 19l9 2-9-18-9 18 9-2zm0 0v-8" />
                </svg>
                <p class="pt-5 font-semibold">EASY CUSTOMIZATION</p>
                <div class="bg-teal-500 w-20 rounded-full mx-auto mt-4 h-1"></div>
            </div>
            <div class="pt-5">
                <p class="text-sm">Fortuna features a wide variety of theme and element options are at your disposal
                    so you never have to worry about being unique.

                </p>
            </div>
        </div>
        <div class="text-center">
            <div class="">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-32 mx-auto text-white bg-orange-400 p-6 rounded-full w-32" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M11 4a2 2 0 114 0v1a1 1 0 001 1h3a1 1 0 011 1v3a1 1 0 01-1 1h-1a2 2 0 100 4h1a1 1 0 011 1v3a1 1 0 01-1 1h-3a1 1 0 01-1-1v-1a2 2 0 10-4 0v1a1 1 0 01-1 1H7a1 1 0 01-1-1v-3a1 1 0 00-1-1H4a2 2 0 110-4h1a1 1 0 001-1V7a1 1 0 011-1h3a1 1 0 001-1V4z" />
                </svg>
                <p class="pt-5 font-semibold">SMOOTH SETUP</p>
                <div class="bg-teal-500 w-20 mx-auto rounded-full mt-4 h-1"></div>
            </div>
            <div class="pt-5">
                <p class="text-sm">Fortuna is very easy to install and setup. You install the Theme and its plugins,
                    then import the Demos and you are good to go.</p>
            </div>
        </div>
    </div>
</div>