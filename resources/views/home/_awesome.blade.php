<div class="pt-0 lg:p-10 md:p-5 p-0">
    <div class="lg:flex-row justify-center items-center flex md:flex-col flex-col">
        <div class="lg:w-6/12 md:w-full w-full">
            <img class="w-full md:h-96 h-96 mx-auto lg:h-full" src="images/ex.jpg">
        </div>
        <div class="p-5 pt-20 lg:w-6/12 md:w-full w-full mx-auto">
            <div class="">
                <p class="lg:text-4xl md:text-2xl text-2xl">We are<span class="text-teal-500" style="font-size:32px;top:0;">Awesome</span>
                </p>
            </div>
            <div class="lg:w-9/12 md:w-full w-full pt-10">
                <p>We are a young team of professionals that love and take pride in their work. Let’s work together!
                </p>
            </div>
            <div class="flex items-center pt-10">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-8 text-teal-500 w-8" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM9.555 7.168A1 1 0 008 8v4a1 1 0 001.555.832l3-2a1 1 0 000-1.664l-3-2z" clip-rule="evenodd" />
                </svg>
                <p class="text-teal-500 mx-3">Watch Viedo</p>
            </div>
        </div>
    </div>
</div>