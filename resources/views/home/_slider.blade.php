<div class="">
    <div class="slideshow-container">

        <div class="mySlides fade">
            <!-- <div class="numbertext">1 / 3</div> -->
            <div class="relative">
                <img src="images/black.jpg" style="width:100%;height:100%;">
                <!-- <div class="text">Caption Text</div> -->
                <div class="text-center flex items-center justify-center flex-col absolute w-full h-full top-0">
                    <p class=" lg:top-44 md:top-80 top-20 text-white lg:text-4xl md:text-4xl text-xl  right-0 left-0">CLEAN DESIGN. <span class="text-orange-300" style="top:0;font-size:24px">CLEAR</span>
                        MESSAGE</p>
                    <i class=" lg:top-56 md:top-96 top-28 text-center font-semibold text-white  right-0 left-0">We are a passionate
                        about the
                        web
                        bunch of fellas <br>that love & take pride in their work</i>
                </div>
            </div>

        </div>

        <div class="mySlides fade">
            <div class="relative">
                <img src="images/big.JPG" style="width:100%;height:100%;">
                <!-- <div class="text">Caption Text</div> -->
                <div class="text-center flex items-center justify-center flex-col absolute w-full h-full top-0">
                    <p class=" absolute lg:top-80 md:top-80 top-20 text-white lg:text-4xl md:text-4xl text-xl right-0 left-0">CLEAN DESIGN. <span class="text-orange-300">CLEAR</span>
                        MESSAGE</p>
                    <i class=" absolute lg:top-96 md:top-96 top-28 text-center font-semibold text-white  right-0 left-0">We are a passionate
                        about the
                        web
                        bunch of fellas <br>that love & take pride in their work</i>
                </div>
            </div>

        </div>

        <div class="mySlides fade">
            <div class="relative">
                <img src="images/d.JPEG" style="width:100%;height:100%;">
                <!-- <div class="text">Caption Text</div> -->
                <div class="text-center flex items-center justify-center flex-col absolute w-full h-full top-0">
                    <p class=" absolute lg:top-80 md:top-80 top-20 text-white lg:text-4xl md:text-4xl text-xl right-0 left-0">CLEAN DESIGN. <span class="text-orange-300">CLEAR</span>
                        MESSAGE</p>
                    <i class=" absolute lg:top-96 md:top-96 top-28 text-center font-semibold text-white  right-0 left-0">We are a passionate
                        about the
                        web
                        bunch of fellas <br>that love & take pride in their work</i>
                </div>
            </div>

        </div>

        <a class="prev" onclick="plusSlides(-1)">❮</a>
        <a class="next" onclick="plusSlides(1)">❯</a>

    </div>
    <br>

    <div style="text-align:center">
        <span class="dot" onclick="currentSlide(1)"></span>
        <span class="dot" onclick="currentSlide(2)"></span>
        <span class="dot" onclick="currentSlide(3)"></span>
    </div>
</div>
<div class="bg-gray-100 shadow">
    <marquee width="100%" class="mt-6 text-sm font-semibold text-gray-500" onmouseover="this.stop();" onmouseout="this.start();" direction="left" height="50px">
        This is a sample scrolling text that has scrolls in the upper direction.
    </marquee>
</div>