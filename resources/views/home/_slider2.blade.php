<div class="owl-slider mt-20 bg-gray-800/50 border-line" style="background-image:url(images/st.jpg);background-size:cover;background-repeat:no-repeat;font-family: 'Aboreto', cursive;">
    <p class="text-center font-bold  pt-20">WHAT OUR<span class="text-teal-500" style="top: 0;font-size:24px;">CLIENT </span> SAYS</p>
    <div class="bg-teal-500 w-20 rounded-full mx-auto mt-4 h-1"></div>
    <div id="carousel_testimonial" class="owl-carousel">
        <div class="item lg:w-6/12 m:w-full w-full mx-auto p-5 item-center justify-center">
            <div class="text-center text-white p-10">
                <i class="text-xl">"Professionally cultivate one-to-one customer service with robust ideas.
                    dynamically innovate resource-levelling customer service for state of the art customer
                    service"</i>
            </div>
            <div class="flex items-center justify-center">
                <img class="h-48 img_size ring-white ring-2 rounded-full " src="images/alen.jpg">
            </div>
            <div class="text-center text-white pt-6">
                <p class="font-bold  text-2xl">DAVID S. MORRIS</p>
                <i>Marketing Manager</i>
            </div>

        </div>
        <div class="item lg:w-6/12 m:w-full w-full mx-auto  p-5">
            <div class="text-center text-white p-10">
                <p class="lg:text-xl md:text-xl text-xs">"Solid performance and amazing support! Dramatically
                    maintain clicks-
                    and-mortar solutions without functional solutions"</p>
            </div>
            <div class="flex items-center justify-center">
                <img class="h-48  ring-white img_size ring-2 rounded-full " src="images/Alan-Smith.jpg" style="width: 27%;">
            </div>
            <div class="text-center text-white pt-6">
                <p class="font-bold lg:text-2xl md:text-2xl text-xl">DALMAR JOHNSON</p>
                <i>Marketing Manager</i>
            </div>

        </div>
        <div class="item lg:w-6/12 m:w-full w-full mx-auto  p-5">
            <div class="text-center text-white p-10">
                <p class="text-xl">"Quickly maximize timely delivarables for real-time schemas.
                    Dramatically maintain clicks-and-mortar solutions without functional solutions".</p>
            </div>
            <div class="flex items-center justify-center">
                <img class=" img_size h-48 ring-white ring-2 rounded-full " src="images/aisy.jpg" style="width: 27%;">
            </div>
            <div class="text-center text-white pt-6">
                <p class="font-bold text-2xl">RICHARD M. BLALOCK</p>
                <i>Designer</i>
            </div>

        </div>
    </div>
</div>