<div class="mt-40 lg:p-2 md:p-2 p-0">
    <div class="lg:flex md:flex-card flex-card">
        <div class="lg:w-6/12 md:w-full w-full mx-auto">
            <img class="w-full h-96" src="images/33.jpg">
        </div>
        <div class="lg:w-6/12 md:w-full w-full mx-auto p-10">
            <div class="">
                <p class="lg:text-4xl md:text-2xl text-2xl">Your ultimate<span class="text-teal-500 " style="font-size: 35px;top:0;">WordPress</span> Theme</p>
                <i class="text-gray-300 mt-3">Build your website today in a matter of minutes</i>
            </div>
            <div class="pt-8">
            </div>
            <p class="w-full">The best of the best is combined in Fortuna. It features fine aesthetics, strong functional backend,
                plenty of customizable elements, a vast array of theme customization options and last but not least
                a 5 star support.</p>
            <div class="mt-10">
                <a href="#" class="py-3 rounded px-6 hover:bg-black bg-teal-500 text-xs font-bold text-white">PURCHASE
                    NOW</a>
            </div>
        </div>
    </div>
</div>