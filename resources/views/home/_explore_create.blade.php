<div class="container mx-auto pt-10">
    <div class="lg:flex-row justify-center items-center flex md:flex-col flex-col">
        <div class="lg:w-7/12 md:w-full w-full mx-auto lg:pt-40 md:pt-30 pt-0  lg:p-10 md:p-5 p-2">
            <div class="">
                <p class="text-4xl">Explore. <span class="text-teal-500" style="font-size: 32px;top:0%;">Design.</span>
                    Create.</p>
                <p>Build your Website in a matter of minutes</p>
            </div>
            <div class="lg:p-10 md:p- pt-5">
                <div class="lg:flex md:flex-card flex-card">
                    <div class="">
                        <img class="lg:w-40 md:w-14 w-14" src="images/pencil-and-ruler.png">
                    </div>
                    <div class="lg:mx-10 md:mx-0 mx-0 lg:pt-0 md:pt-2 pt-5">
                        <p>DESIGNED TO AMAZE</p>
                        <p>Fortuna features a characteristic clean, modern design along with a wide variety of theme
                            options
                            so you will always be unique.</p>
                    </div>
                </div>
                <div class="lg:flex md:flex-card flex-card pt-10">
                    <div class="">
                        <img class="lg:w-40 md:w-14 w-14" src="images/bullseye-with-arrow.png">
                    </div>
                    <div class="lg:mx-10 md:mx-0 mx-0 lg:pt-0 md:pt-2 pt-5">
                        <p>TOP NOTCH FUNCTIONALITY</p>
                        <p>Do not be fooled by its looks. Fortuna has a rock-solid backend functionality and is a
                            heavy weight lifter by all standards.</p>
                    </div>
                </div>
                <div class="lg:flex md:flex-card flex-card pt-10">
                    <div class="">
                        <img class="lg:w-40 md:w-14 w-14" src="images/anchor.png">
                    </div>
                    <div class="lg:mx-10 md:mx-0 mx-0 lg:pt-0 md:pt-2 pt-5">
                        <p>YOU ARE IN GOOD HANDS</p>
                        <p>Last but not least Fortuna ships with our 5-star support that you can count on with any
                            questions you may have.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="w-6/12 mx-auto mt-20">
            <img class="w-full h-5/6" src="images/fr.jpg">
        </div>
    </div>
</div>