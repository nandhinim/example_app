<div class="lg:flex md:flex-card text-white flex-card" style="background-image:url(images/istockphoto-1345979900-612x612.jpg);background-size:cover;background-repeat:no-repeat">
    <div class="grid grid-cols-1 lg:grid-cols-2 md:grid-cols-2 gap-4 lg:w-8/12 md:w-full w-full mx-auto bg-gray-800/90 lg:p-20 md:p-5 p-5">
        <div class="flex">
            <div class="">
                <img class="lg:w-40 md:w-20 w-20" src="images/pencil-and-ruler.png">
            </div>
            <div class="mx-6">
                <p>STUNNING DESIGN</p>
                <p>Fortuna is a clean design, powerful WordPress Template suitable for a wide variety of websites
                    that both you and your clients will fall in love with</p>
            </div>
        </div>
        <div class="flex">
            <div class="">
                <img class="w-40" src="images/atom.png">
            </div>
            <div class="mx-6">
                <p>STUNNING DESIGN</p>
                <p>Fortuna is a clean design, powerful WordPress Template suitable for a wide variety of websites
                    that both you and your clients will fall in love with</p>
            </div>
        </div>
        <div class="flex">
            <div class="">
                <img class="w-40" src="images/settings.png">
            </div>
            <div class="mx-6">
                <p>STUNNING DESIGN</p>
                <p>Fortuna is a clean design, powerful WordPress Template suitable for a wide variety of websites
                    that both you and your clients will fall in love with</p>
            </div>
        </div>
        <div class="flex">
            <div class="">
                <img class="w-40" src="images/anchor.png">
            </div>
            <div class="mx-6">
                <p>STUNNING DESIGN</p>
                <p>Fortuna is a clean design, powerful WordPress Template suitable for a wide variety of websites
                    that both you and your clients will fall in love with</p>
            </div>
        </div>
    </div>
    <div class="lg:w-4/12 md:w-full w-full bg-teal-400/50">
        <div class="lg:pt-48 md:p-10 p-10 flex">
            <img class="w-1/2 mx-auto  h-10" src="images/c.png">
        </div>
    </div>
</div>