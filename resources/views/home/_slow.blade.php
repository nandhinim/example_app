<div class="p-10 mt-10 mx-auto bg-gray-100">
    <div class="grid grid-cols-2 lg:grid-cols-4 md:grid-cols-3 gap-4">
        <div class="text-center">
            <div class="">
                <img class="lg:w-20 md:w-14 md:h-14 w-14 h-14 mx-auto lg:h-20" src="images/mobile-phone.png">
            </div>
            <div class="pt-8">
                <p class="lg:text-4xl md:text-2xl text-2xl">246</p>
                <div class="pt-6">
                    <p class="lg:text-xl md:text-sm text-sm">Mobile Users</p>
                </div>
            </div>
        </div>
        <div class="text-center">
            <div data-aos="fade-up" data-aos-duration="3000" class="">
                <img class="lg:w-20 md:w-14 md:h-14 w-14 h-14 mx-auto lg:h-20" src="images/diamond.png">
            </div>
            <div data-aos="fade-down" class="pt-8">
                <p class="lg:text-4xl md:text-2xl text-2xl">712</p>
                <div class="pt-6">
                    <p class="lg:text-xl md:text-sm text-sm">Slick Effects</p>
                </div>
            </div>
        </div>
        <div class="text-center">
            <div data-aos-duration="3000" class="">
                <img class="lg:w-20 md:w-14 md:h-14 w-14 h-14 mx-auto lg:h-20" src="images/equalizer-control.png">
            </div>
            <div class="pt-8">
                <p class="lg:text-4xl md:text-2xl text-2xl">127</p>
                <div class="pt-6">
                    <p class="lg:text-xl md:text-sm text-sm">Custom Options</p>
                </div>
            </div>
        </div>
        <div class="text-center">
            <div class="">
                <img class="lg:w-20 md:w-14 md:h-14 w-14 h-14 mx-auto lg:h-20" src="images/truck.png">
            </div>
            <div class="pt-8">
                <p class="lg:text-4xl md:text-2xl text-2xl">246</p>
                <div class="pt-6">
                    <p class="lg:text-xl md:text-sm text-sm">Presets Available</p>
                </div>
            </div>
        </div>
    </div>
</div>