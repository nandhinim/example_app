<div class="filters filter-button-group">
        <ul class="un">
            <h4>
                <li class="active le" data-filter="*">All</li>
                <li data-filter=".webdesign" class="le">Graphic Design</li>
                <li data-filter=".webdev" class="le">Viedo Animation</li>
                <li data-filter=".brand" class="le">Web Design</li>
                <li data-filter=".brand" class="le">Web Development</li>
                <li data-filter=".brand" class="le">Wordpress
                    Themes</li>
            </h4>
        </ul>
    </div>

    <div class="content filter-grid grid">

        <div class="single-content webdesign webdev grid-item">
            <div  class="over_img">
                <img src="images/1.jpg" alt="Avatar" class="image h-44 mx-auto w-60">
                <div class="overlay text-center ">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-auto mt-6 w-6" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round"
                            d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z" />
                        <path stroke-linecap="round" stroke-linejoin="round" d="M15 13a3 3 0 11-6 0 3 3 0 016 0z" />
                    </svg>
                    <p class="text-xs mt-3">BRAND CREATION PROJECT</p>
                    <i class="text-xs mt-3">Graphic Design/Viedo Animation</i>
                </div>
            </div>
        </div>

        <div class="single-content brand webdesign grid-item">
            <div  class="over_img">
                <img src="images/3.jpg" alt="Avatar" class="image h-44 mx-auto w-60">
                <div class="overlay text-center ">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-auto mt-4 w-6" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round"
                            d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z" />
                        <path stroke-linecap="round" stroke-linejoin="round" d="M15 13a3 3 0 11-6 0 3 3 0 016 0z" />
                    </svg>
                    <p class="text-xs mt-3">MOBILE PROJECT</p>
                    <i class="text-xs mt-3">Viedo Animation/Web Development</i>
                </div>
            </div>
        </div>

        <div class="single-content brand grid-item">
            <div  class="over_img">
                <img src="images/4.webp" alt="Avatar" class="image h-44 mx-auto w-60">
                <div class="overlay text-center ">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-auto mt-4 w-6" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round"
                            d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z" />
                        <path stroke-linecap="round" stroke-linejoin="round" d="M15 13a3 3 0 11-6 0 3 3 0 016 0z" />
                    </svg>
                    <p class="text-xs mt-3">CUSTOM TEMPLATE ITEM</p>
                    <i class="text-xs mt-3">Graphic Design/Wordpress Themes</i>
                </div>
            </div>
        </div>

        <div class="single-content webdesign grid-item">
            <div class="over_img">
                <img src="images/5.jpg" alt="Avatar" class="image h-44 mx-auto w-60">
                <div class="overlay text-center ">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-auto mt-4 w-6" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round"
                            d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z" />
                        <path stroke-linecap="round" stroke-linejoin="round" d="M15 13a3 3 0 11-6 0 3 3 0 016 0z" />
                    </svg>
                    <p class="text-xs mt-3">COOL PORTFOLIO ITEM</p>
                    <P class="text-xs mt-3">Graphic Design/Viedo Animation</P>
                </div>
            </div>
        </div>

        <div class="single-content webdesign grid-item">
            <div  class="over_img">
                <img src="images/6.jpg" alt="Avatar" class="image h-44 mx-auto w-60">
                <div class="overlay text-center ">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-auto mt-4 w-6" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round"
                            d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z" />
                        <path stroke-linecap="round" stroke-linejoin="round" d="M15 13a3 3 0 11-6 0 3 3 0 016 0z" />
                    </svg>
                    <p class="text-xs mt-3">HOT SINGLE IMAGE PROJECT</p>
                    <i class="text-xs mt-3">Viedo Animation/Web Design</i>
                </div>
            </div>
        </div>

        <div class="single-content webdesign brand grid-item">
            <div  class="over_img">
                <img src="images/7.jpg" alt="Avatar" class="image h-44 mx-auto w-60">
                <div class="overlay text-center ">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-auto mt-4 w-6" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round"
                            d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z" />
                        <path stroke-linecap="round" stroke-linejoin="round" d="M15 13a3 3 0 11-6 0 3 3 0 016 0z" />
                    </svg>
                    <p class="text-xs mt-3">SOME COOL PORTFOLIO ITEM</p>
                    <i class="text-xs mt-3">Web Development/Wordpress Themes</i>
                </div>
            </div>
        </div>

        <div class="single-content webdesign grid-item">
            <div  class="over_img">
                <img src="images/8.jpg" alt="Avatar" class="image h-44 mx-auto w-60">
                <div class="overlay text-center ">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-auto mt-4 w-6" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round"
                            d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z" />
                        <path stroke-linecap="round" stroke-linejoin="round" d="M15 13a3 3 0 11-6 0 3 3 0 016 0z" />
                    </svg>
                    <p class="text-xs mt-3">SINGLE IMAGE PROJECT</p>
                    <i class="text-xs mt-3">Graphic Design/Viedo Animation/Web Design</i>
                </div>
            </div>
        </div>

        <div class="single-content webdesign webdev grid-item">
            <div class="over_img">
                <img src="images/9.jpg" alt="Avatar" class="image h-44 mx-auto w-60">
                <div class="overlay text-center ">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-auto mt-4 w-6" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round"
                            d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z" />
                        <path stroke-linecap="round" stroke-linejoin="round" d="M15 13a3 3 0 11-6 0 3 3 0 016 0z" />
                    </svg>
                    <p class="text-xs mt-3">EXCITING PROJECT</p>
                    <P class="text-xs mt-3">Web Development/Wordpress Theme</P>
                </div>
            </div>
        </div>
        <div class="single-content webdesign webdev grid-item">
            <div class="over_img">
                <img src="images/10.webp" alt="Avatar" class="image h-44 mx-auto w-60">
                <div class="overlay text-center ">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-auto mt-4 w-6" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round"
                            d="M3 9a2 2 0 012-2h.93a2 2 0 001.664-.89l.812-1.22A2 2 0 0110.07 4h3.86a2 2 0 011.664.89l.812 1.22A2 2 0 0018.07 7H19a2 2 0 012 2v9a2 2 0 01-2 2H5a2 2 0 01-2-2V9z" />
                        <path stroke-linecap="round" stroke-linejoin="round" d="M15 13a3 3 0 11-6 0 3 3 0 016 0z" />
                    </svg>
                    <p class="text-xs mt-3">AMAZING PORTFOLIO ITEM</p>
                    <P class="text-xs mt-3">Graphic Design/Web Design</P>
                </div>
            </div>
        </div>



    </div>