<div class=" bg-gray-100 p-6">
    <div class="container mx-auto">
        <div class="flex lg:flex-row md:flex-col flex-col justify-center items-center">
            <div class="lg:w-6/12 md:w-full w-full pt-3 mx-auto">
                <img src="images/iphone.png">
            </div>
            <div class="lg:w-6/12 md:w-full w-full pt-10 mx-auto">
                <p class="lg:text-4xl md:text-2xl text-2xl">Fully <span class="text-cyan-400" style="top: 0; font-size:37px">Responsive</span>
                    Awesomeness</p>
                <i class="mt-2 text-gray-300 font-semibold">Fortuna’s liquid design flows smoothly on all size
                    devices</i>
                <div class="pt-4">
                    <p>Not only is Fortuna fully responsive and looks great on different devices but you can customize
                        each element’s behavior on distinct devices within the easy to manage Visual Composer responsive
                        options window.</p>
                </div>
                <div class="mt-10">
                    <div class="justify-center items-center flex bg-teal-300 px-5 py-2 rounded text-xs text-white font-semibold w-fit">
                        <a href="#" class="">ALL FEATURES </a>
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 mx-2 h-4">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>