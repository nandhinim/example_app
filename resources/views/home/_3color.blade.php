<div class="pt-20">
    <div class="grid grid-cols-1 lg:grid-cols-3 md:grid-cols-2">
        <div class="bg-lime-300 lg:p-20 md:p-5 p-5 text-white">
            <div class="text-2xl">
                <p>WE CRAFT BEAUTIFUL SITES</p>
            </div>
            <div class="bg-gray-200 w-16 mt-6 h-0.5">
            </div>
            <div class="pt-6">
                <p>You and your clients will love the exceptionally beautiful clean and simple design of Fortuna. Ut
                    elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
            </div>
            <div class="pt-5">
                <a href="#" class="flex items-center justify-center px-2 rounded border border-white text-white hover:bg-white hover:text-black text-xs font-semibold w-40 h-10">CONTACT
                    US<svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-3 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M14 10h4.764a2 2 0 011.789 2.894l-3.5 7A2 2 0 0115.263 21h-4.017c-.163 0-.326-.02-.485-.06L7 20m7-10V5a2 2 0 00-2-2h-.095c-.5 0-.905.405-.905.905 0 .714-.211 1.412-.608 2.006L7 11v9m7-10h-2M7 20H5a2 2 0 01-2-2v-6a2 2 0 012-2h2.5" />
                    </svg></a>
            </div>
        </div>
        <div class="bg-teal-500 lg:p-20 md:p-5 p-5 text-white">
            <div class="text-2xl">
                <p>WE BUILD STRONG BACKENDS
                </p>
            </div>
            <div class="bg-gray-200 w-16 mt-6 h-0.5">
            </div>
            <div class="pt-6">
                <p>Fortuna WordPress Theme is rich in Shortcodes and features that are very intuitive and easy to
                    implement. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
            </div>
            <div class="pt-5">
                <a href="#" class="flex items-center px-2 justify-center rounded border border-white text-white hover:bg-white hover:text-black text-xs font-semibold w-40 h-10">CONTACT
                    US<svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-3 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                    </svg></a>
            </div>
        </div>
        <div class="bg-gray-700 lg:p-20 md:p-5 p-5 text-white">
            <div class="text-2xl">
                <p>WE PROVIDE QUALITY SUPPORT</p>
            </div>
            <div class="bg-gray-200 w-16 mt-6 h-0.5">
            </div>
            <div class="pt-6">
                <p>Fortuna is an extremely feature rich WordPress Template designed for a pleasant user experience.
                    Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
            </div>
            <div class="pt-5">
                <a href="#" class="flex items-center px-2 justify-center rounded border border-white text-white hover:bg-white hover:text-black text-xs font-semibold w-40 h-10">CONTACT
                    US<svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-3 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M12 19l9 2-9-18-9 18 9-2zm0 0v-8" />
                    </svg></a>
            </div>
        </div>
    </div>
</div>