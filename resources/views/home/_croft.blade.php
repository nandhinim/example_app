<div class="pt-10 ">
    <!-- <div class="">
            <p>FORTUNA IS AWESOME</p>
        </div> -->
    <div class="lg:flex-row flex justify-center bg-gray-800 items-center md:flex-col flex-col">
        <div class=" lg:w-7/12 md:w-full w-full mx-auto lg:p-10 md:p-5 p-8">
            <div class="text-3xl text-white text-center">
                <p class="">Fortuna is<span class="text-teal-500 mx-2" style="font-size:24px; top:0;">Awesome</span></p>
            </div>
            <div class="bg-teal-500 w-20 rounded-full mx-auto mt-4 h-1"></div>
            <div class="grid grid-cols-1 lg:grid-cols-2 md:grid-cols-2 mt-20 gap-4">
                <div class="lg:flex md:flex-card flex-card items-center justify-center">
                    <div class="">
                        <img class="lg:w-40 md:w-14 w-14" src="images/pencil-and-ruler.png">
                    </div>
                    <div class="lg:mx-6 md:mx-0 mx-0 lg:pt-0 md:pt-5 pt-5 text-white">
                        <p>STUNNING DESIGN</p>
                        <p class="text-sm">Fortuna is a clean design, powerful WordPress Template suitable for a wide
                            variety of
                            websites
                            that both you and your clients will fall in love with</p>
                    </div>
                </div>
                <div class="lg:flex md:flex-card flex-card items-center justify-center">
                    <div class="">
                        <img class="lg:w-40 md:w-14 w-14" src="images/atom.png">
                    </div>
                    <div class="lg:mx-6 md:mx-0 mx-0 lg:pt-0 md:pt-5 pt-5 text-white">
                        <p>STUNNING DESIGN</p>
                        <p class="text-sm">Fortuna is a clean design, powerful WordPress Template suitable for a wide
                            variety of
                            websites
                            that both you and your clients will fall in love with</p>
                    </div>
                </div>
                <div class="lg:flex md:flex-card flex-card mt-5 items-center justify-center">
                    <div class="">
                        <img class="lg:w-40 md:w-14 w-14" src="images/settings.png">
                    </div>
                    <div class="lg:mx-6 md:mx-0 mx-0 lg:pt-0 md:pt-5 pt-5 text-white">
                        <p>STUNNING DESIGN</p>
                        <p class="text-sm">Fortuna is a clean design, powerful WordPress Template suitable for a wide
                            variety of
                            websites
                            that both you and your clients will fall in love with</p>
                    </div>
                </div>
                <div class="lg:flex md:flex-card flex-card mt-5 items-center justify-center">
                    <div class="">
                        <img class="lg:w-40 md:w-4 w-14" src="images/anchor.png">
                    </div>
                    <div class="lg:mx-6 md:mx-0 mx-0 lg:pt-0 md:pt-5 pt-5 text-white">
                        <p>STUNNING DESIGN</p>
                        <p class="text-sm">Fortuna is a clean design, powerful WordPress Template suitable for a wide
                            variety of
                            websites
                            that both you and your clients will fall in love with</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="lg:w-5/12 md:w-full w-full">
            <img class="w-full h-9/12" src="images/aisy.jpg">
        </div>
    </div>
</div>