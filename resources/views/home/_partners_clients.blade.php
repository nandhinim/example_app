<div class="lg:flex md:flex-card flex-card pt-10">
    <div class="lg:w-3/12 md:w-full w-full mx-auto p-10">
        <p class="text-3xl">PARTNERS &
            CLIENTS</p>
        <i class="text-gray-300">Our awesome clients are Awesome!</i>
        <div class="pt-10">
            <p>I am so clever that sometimes I do not get a single word of what I am saying and get very confused.
                Sometimes I worry about being a success in a mediocre world.
            </p>
            <div class="pt-3">
                <p> I am so clever that sometimes I do not get a single word of what I am saying and get very
                    confused.</p>
            </div>
        </div>
    </div>
    <div class="grid grid-cols-2 lg:grid-cols-3 md:grid-cols-3 gap-4 lg:w-7/12 md:w-full w-full bg-white shadow lg:p-20 md:p-10 p-10">
        <div class="">
            <img class="w-20 h-20" src="images/tiktok.png">
        </div>
        <div class="">
            <img class="w-20 h-20" src="images/apple.png">
        </div>
        <div class="">
            <img class="w-20 h-20" src="images/photoshop.png">
        </div>
        <div class="">
            <img class="w-20 h-20" src="images/telegram.png">
        </div>
        <div class="">
            <img class="w-20 h-20" src="images/app-store.png">
        </div>
        <div class="">
            <img class="w-20 h-20" src="images/slack.png">
        </div>
        <div class="">
            <img class="w-20 h-20" src="images/instagram.png">
        </div>
        <div class="">
            <img class="w-20 h-20" src="images/heart-beats.png">
        </div>
        <div class="">
            <img class="w-20 h-20" src="images/gmail.png">
        </div>
    </div>
</div>