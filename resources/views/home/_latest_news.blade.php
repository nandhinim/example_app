<div class="container mx-auto">
    <div class="text-center hii pt-20 flex">
        <p class="w-40 mx-auto">LATEST NEWS</p>
        <!-- <div class="">
                <div class="owl-nav"><button type="button" role="presentation" class=""><span
                            aria-label="Previous">‹</span></button><button type="button" role="presentation"
                        class="owl-next hii"><span aria-label="Next">›</span></button></div>
            </div> -->
    </div>
</div>
<div class="owl-slider pt-10">
    <div id="carousel" class="owl-carousel">
        <div class="item w-11/12 mx-auto">
            <div class="flex items-center justify-center">
                <img class=" h-36" src="images/1.jpg" style="width: 70%;">
            </div>
            <div class="text-center pt-6">
                <div class="">
                    <p class="text-xl">Latest New Post</p>
                </div>
                <div class="flex items-center pt-4 justify-center">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-2 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                    </svg>
                    <i>April 23,2015</i>
                </div>
                <div class="pt-5">
                    <p>Lorem ipsum dolor sit amet, consect rthmh eof retj oigtrm fgnmtg ekofo rfewomi3gs</p>
                </div>
                <div class="pt-3">
                    <a href="#" class="text-xs items-center justify-center flex"><svg xmlns="http://www.w3.org/2000/svg" class="h-5 text-teal-500 w-5" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                        </svg>Read More</a>
                </div>
            </div>
        </div>
        <div class="item w-11/12 mx-auto">
            <div class="flex items-center justify-center">
                <img class=" h-36" src="images/h2.jpg" style="width: 70%;">
            </div>
            <div class="text-center pt-6">
                <div class="">
                    <p class="text-xl">Latest New Post</p>
                </div>
                <div class="flex items-center pt-4 justify-center">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-2 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                    </svg>
                    <i>April 23,2015</i>
                </div>
                <div class="pt-5">
                    <p>Lorem ipsum dolor sit amet, consect rthmh eof retj oigtrm fgnmtg ekofo rfewomi3gs</p>
                </div>
                <div class="pt-3">
                    <a href="#" class="text-xs items-center justify-center flex"><svg xmlns="http://www.w3.org/2000/svg" class="h-5 text-teal-500 w-5" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                        </svg>Read More</a>
                </div>
            </div>
        </div>
        <div class="item w-11/12 mx-auto">
            <div class="flex items-center justify-center">
                <img class=" h-36" src="images/9.jpg" style="width: 70%;">
            </div>
            <div class="text-center pt-6">
                <div class="">
                    <p class="text-xl">Latest New Post</p>
                </div>
                <div class="flex items-center pt-4 justify-center">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-2 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                    </svg>
                    <i>April 23,2015</i>
                </div>
                <div class="pt-5">
                    <p>Lorem ipsum dolor sit amet, consect rthmh eof retj oigtrm fgnmtg ekofo rfewomi3gs</p>
                </div>
                <div class="pt-3">
                    <a href="#" class="text-xs items-center justify-center flex"><svg xmlns="http://www.w3.org/2000/svg" class="h-5 text-teal-500 w-5" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                        </svg>Read More</a>
                </div>
            </div>
        </div>
        <div class="item w-11/12 mx-auto">
            <div class="flex items-center justify-center">
                <img class=" h-36" src="images/h1.jpg" style="width: 70%;">
            </div>
            <div class="text-center pt-6">
                <div class="">
                    <p class="text-xl">Latest New Post</p>
                </div>
                <div class="flex items-center pt-4 justify-center">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-2 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                    </svg>
                    <i>April 23,2015</i>
                </div>
                <div class="pt-5">
                    <p>Lorem ipsum dolor sit amet, consect rthmh eof retj oigtrm fgnmtg ekofo rfewomi3gs</p>
                </div>
                <div class="pt-3">
                    <a href="#" class="text-xs items-center justify-center flex"><svg xmlns="http://www.w3.org/2000/svg" class="h-5 text-teal-500 w-5" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                        </svg>Read More</a>
                </div>
            </div>
        </div>
        <div class="item w-11/12 mx-auto">
            <div class="flex items-center justify-center">
                <img class=" h-36" src="images/33.jpg" style="width: 70%;">
            </div>
            <div class="text-center pt-6">
                <div class="">
                    <p class="text-xl">Latest New Post</p>
                </div>
                <div class="flex items-center pt-4 justify-center">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-2 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                    </svg>
                    <i>April 23,2015</i>
                </div>
                <div class="pt-5">
                    <p>Lorem ipsum dolor sit amet, consect rthmh eof retj oigtrm fgnmtg ekofo rfewomi3gs</p>
                </div>
                <div class="pt-3">
                    <a href="#" class="text-xs items-center justify-center flex"><svg xmlns="http://www.w3.org/2000/svg" class="h-5 text-teal-500 w-5" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                        </svg>Read More</a>
                </div>
            </div>
        </div>
        <div class="item w-11/12 mx-auto">
            <div class="flex items-center justify-center">
                <img class=" h-36" src="images/6.jpg" style="width: 70%;">
            </div>
            <div class="text-center pt-6">
                <div class="">
                    <p class="text-xl">Latest New Post</p>
                </div>
                <div class="flex items-center pt-4 justify-center">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-2 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                    </svg>
                    <i>April 23,2015</i>
                </div>
                <div class="pt-5">
                    <p>Lorem ipsum dolor sit amet, consect rthmh eof retj oigtrm fgnmtg ekofo rfewomi3gs</p>
                </div>
                <div class="pt-3">
                    <a href="#" class="text-xs items-center justify-center flex"><svg xmlns="http://www.w3.org/2000/svg" class="h-5 text-teal-500 w-5" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                        </svg>Read More</a>
                </div>
            </div>
        </div>
        <div class="item w-11/12 mx-auto">
            <div class="flex items-center justify-center">
                <img class=" h-36" src="images/2.jpg" style="width: 70%;">
            </div>
            <div class="text-center pt-6">
                <div class="">
                    <p class="text-xl">Latest New Post</p>
                </div>
                <div class="flex items-center pt-4 justify-center">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-2 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                    </svg>
                    <i>April 23,2015</i>
                </div>
                <div class="pt-5">
                    <p>Lorem ipsum dolor sit amet, consect rthmh eof retj oigtrm fgnmtg ekofo rfewomi3gs</p>
                </div>
                <div class="pt-3">
                    <a href="#" class="text-xs items-center justify-center flex"><svg xmlns="http://www.w3.org/2000/svg" class="h-5 text-teal-500 w-5" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                        </svg>Read More</a>
                </div>
            </div>
        </div>
    </div>
</div>