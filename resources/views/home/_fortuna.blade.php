<div class=" pt-28">
    <div class="lg:flex md:flex-card flex-card">
        <div class=" md:w-full w-full ">
            <img class="w-full md:h-96 h-96 lg:h-full" src="images/44.jpg">
        </div>
        <div class=" md:w-full w-full lg:p-20 md:p-20 p-8  bg-gray-700 text-white text-center ">
            <div class=" lg:text-4xl md:text-2xl text-2xl mt-10">
                <p>Fortuna is simply Awesome</p>
                <!-- <div class="bg-gray-400 w-10 h-0.5 ">
                    </div> -->
            </div>

            <div class="lg:p-10 md:p-5 p-5">
                <p>The best of the best is combined in Fortuna. It features fine aesthetics, strong functional
                    backend, plenty of customizable elements, a vast array of theme customization options and last
                    but not least a 5 star support. Fortuna is extremely diverse and implements Visual Composer.

                </p>
            </div>
            <div class="border-white px-3 py-2 flex rounded-full mx-auto w-fit border-2">
                <a href="#" class=" text-sm ">BUY NOW </a>
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                </svg>

            </div>
        </div>
    </div>
</div>