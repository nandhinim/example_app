<div class="lg:pt-36 md:pt-10 pt-10 p-10">
    <div class="text-center">
        <p>THEME TECHS & SPECS</p>
    </div>
    <div class="lg:pt-20 md:pt-5 pt-5 lg:p-10 md:p-5 p-5 lg:flex-row flex md:flex-col flex-col items-center justify-center">
        <div class="lg:w-6/12 md:w-full w-full p-4 mx-auto">
            <ul>
                <li class="mt-2">
                    <div class="flex mx-4 ">
                        <p>Tools & Functionality</p>
                        <!-- <p class="mx-auto">96%</p> -->
                    </div>
                    <div class="animated-progress progress-blue">
                        <span data-progress="96"></span>
                    </div>

                </li>
                <li class="mt-4">
                    <div class="flex mx-4">
                        <p>Design & Cool Factor</p>
                        <!-- <p class="mx-auto">96%</p> -->
                    </div>

                    <div class="animated-progress progress-green">
                        <span data-progress="80"></span>
                    </div>

                </li>
                <li class="mt-4 ">
                    <div class="flex mx-4">
                        <p>Customization Options</p>
                        <!-- <p class="mx-auto">96%</p> -->
                    </div>
                    <div class="animated-progress progress-purple">
                        <span data-progress="70"></span>
                    </div>

                </li>
                <li class="mt-4">
                    <div class="flex mx-4">
                        <p>Theme Support</p>
                        <!-- <p class="mx-auto">96%</p> -->
                    </div>
                    <div class="animated-progress progress-red">
                        <span data-progress="85"></span>
                    </div>

                </li>
            </ul>
        </div>
        <div x-data="{showAccordion:0}" class=" lg:w-6/12 md:w-full w-full mx-auto py-3">
            <div class=" my-3">
                <div @click="showAccordion = 1" class=" p-3 flex">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 cursor-pointer w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                    </svg>
                    <!--    <div x-text="showAccordion == 1 ? '+' : '-'"></div> -->
                    <div>
                        <p class="mx-4" :class="{'font-bold' : showAccordion == 1}">STUNNING DESIGN & COUNTLESS
                            OPTIONS</p>
                    </div>
                </div>
                <div x-show="showAccordion == 1" class="p-3">
                    Fortuna is a diverse WordPress theme suitable for a wide variety of websites: Corporate,
                    Company,
                    eCommerce, Portfolio, Personal, Blogs. Start our trial today!
                </div>
            </div>
            <div class=" my-3">
                <div @click="showAccordion = 2" class=" p-3 flex ">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 cursor-pointer w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                    </svg>
                    <div>
                        <p class="mx-4" :class="{'font-bold' : showAccordion == 1}">TONS OF CUSTOMIZATION OPTIONS AT
                            YOUR
                            FINGER TIPS</p>
                    </div>
                </div>
                <div x-show="showAccordion == 2" class="p-3">
                    Fortuna will blow your mind with the virtually unlimited customization options it offers.
                    Hundreds of
                    style presets are available to you for all your menus, portfolio items, post items, carousels,
                    grids
                    etc.
                </div>
            </div>
            <div class=" my-3">
                <div @click="showAccordion = 3" class=" p-3 flex">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 cursor-pointer w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                    </svg>
                    <div>
                        <p class="mx-4" :class="{'font-bold' : showAccordion == 1}">EASE OF CONTENT CREATION AND
                            MODIFICATION</p>
                    </div>
                </div>
                <div x-show="showAccordion == 3" class="p-3">
                    Fortuna ships with plenty of customizable interface elements ready to add to your page at the
                    click of a
                    button via the best Drag’n’Drop plugin – Visual Composer.
                </div>
            </div>

        </div>

    </div>
</div>