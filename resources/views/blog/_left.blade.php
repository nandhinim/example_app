<div class="lg:w-8/12 md:w-full w-full mx-auto lg:p-10 md:p-10 p-0 ">
  <div class="">
    <div class="over_img" style="max-width:710px;">
      <img src="images/7.jpg" alt="Avatar" class="image h-auto  w-full">
      <div class="overlay items-center justify-center flex">
        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-auto  w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
          <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
      </div>
    </div>
    <div class="flex pt-10">
      <div class="bg-gray-400 text-center rounded" style="height:80px;">
        <p class=" p-2 rounded">23</p>
        <p class="bg-teal-500 p-2 rounded">Apr</p>
      </div>
      <div class="mx-10 border-b-2 pb-10">
        <a href="{{url('blog-details')}}">LATEST NEWS POST</a>
        <div class="lg:flex-row md:flex-wrap flex flex-wrap mt-4 text-xs text-gray-400">
          <div class="flex ">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.354a4 4 0 110 5.292M15 21H3v-1a6 6 0 0112 0v1zm0 0h6v-1a6 6 0 00-9-5.197M13 7a4 4 0 11-8 0 4 4 0 018 0z" />
            </svg>
            <p class="mx-2">By BlueOwlCreative</p>
          </div>
          <div class="flex mx-2">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z" />
            </svg>
            <p class="mx-2">No comments yet</p>
          </div>
          <div class="flex mx-2">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M7 7h.01M7 3h5c.512 0 1.024.195 1.414.586l7 7a2 2 0 010 2.828l-7 7a2 2 0 01-2.828 0l-7-7A1.994 1.994 0 013 12V7a4 4 0 014-4z" />
            </svg>
            <p class="mx-2">Graphics, Wordpress</p>
          </div>
        </div>
        <div class="lg:w-8/12 md:w-full w-full pt-4">
          <p class="text-sm">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed eleifend urna eu sapien. Quisque posuere nunc eu massa. Praesent bibendum lorem non leo. Morbi volutpat, urna eu fermentum rutrum, ligula lacus interdum mauris, ac pulvinar libero pede a enim. Etiam commodo malesuada ante. Donec nec ligula. Curabitur mollis semper diam.Duis viverra nibh a felis condimentum pretium. Nullam tristique lacus non</p>
        </div>
        <div class="flex items-center pt-4">
          <svg xmlns="http://www.w3.org/2000/svg" class="h-4 text-white rounded bg-teal-500 w-4" viewBox="0 0 20 20" fill="currentColor">
            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
          </svg>
          <p class="mx-3 text-sm">Read more</p>
        </div>
      </div>
    </div>
  </div>
  <!-- 2 line -->
  <div class="pt-4">
    <div class="over_img" style="max-width:710px;">
      <img src="images/h2.jpg" alt="Avatar" class="image h-auto  w-full">
      <div class="overlay items-center justify-center flex ">
        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-auto w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
          <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
      </div>
    </div>
    <div class="flex pt-10">
      <div class="bg-gray-400 text-center rounded" style="height:80px;">
        <p class=" p-2 rounded">20</p>
        <p class="bg-teal-500 p-2 rounded">Apr</p>
      </div>
      <div class="mx-10 border-b-2 pb-10">
        <a href="{{url('blog-details')}}">A STANDARD POST</a>
        <div class="lg:flex-row md:flex-wrap flex flex-wrap mt-4 text-xs text-gray-400">
          <div class="flex ">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.354a4 4 0 110 5.292M15 21H3v-1a6 6 0 0112 0v1zm0 0h6v-1a6 6 0 00-9-5.197M13 7a4 4 0 11-8 0 4 4 0 018 0z" />
            </svg>
            <p class="mx-2">By BlueOwlCreative</p>
          </div>
          <div class="flex mx-2">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z" />
            </svg>
            <p class="mx-2">No comments yet</p>
          </div>
          <div class="flex mx-2">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M7 7h.01M7 3h5c.512 0 1.024.195 1.414.586l7 7a2 2 0 010 2.828l-7 7a2 2 0 01-2.828 0l-7-7A1.994 1.994 0 013 12V7a4 4 0 014-4z" />
            </svg>
            <p class="mx-2">Graphics, Wordpress</p>
          </div>
        </div>
        <div class="lg:w-8/12 md:w-full w-full pt-4">
          <p class="text-sm">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed eleifend urna eu sapien. Quisque posuere nunc eu massa. Praesent bibendum lorem non leo. Morbi volutpat, urna eu fermentum rutrum, ligula lacus interdum mauris, ac pulvinar libero pede a enim. Etiam commodo malesuada ante. Donec nec ligula. Curabitur mollis semper diam.Duis viverra nibh a felis condimentum pretium. Nullam tristique lacus non</p>
        </div>
        <div class="flex items-center pt-4">
          <svg xmlns="http://www.w3.org/2000/svg" class="h-4 text-white rounded bg-teal-500 w-4" viewBox="0 0 20 20" fill="currentColor">
            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
          </svg>
          <p class="mx-3 text-sm">Read more</p>
        </div>
      </div>
    </div>
  </div>
  <div class="pt-4">
    <div class="over_img" style="max-width:710px;">
      <img src="images/44.jpg" alt="Avatar" class="image h-auto  w-full">
      <div class="overlay items-center justify-center flex">
        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-auto w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
          <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
      </div>
    </div>
    <div class="flex pt-10">
      <div class="bg-gray-400 text-center rounded" style="height:80px;">
        <p class=" p-2 rounded">20</p>
        <p class="bg-teal-500 p-2 rounded">Apr</p>
      </div>
      <div class="mx-10 border-b-2 pb-10">
        <a href="{{url('blog-details')}}">COOL VIDEO POST</a>
        <div class="lg:flex-row md:flex-wrap flex flex-wrap mt-4 text-xs text-gray-400">
          <div class="flex ">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.354a4 4 0 110 5.292M15 21H3v-1a6 6 0 0112 0v1zm0 0h6v-1a6 6 0 00-9-5.197M13 7a4 4 0 11-8 0 4 4 0 018 0z" />
            </svg>
            <p class="mx-2">By BlueOwlCreative</p>
          </div>
          <div class="flex mx-2">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z" />
            </svg>
            <p class="mx-2">No comments yet</p>
          </div>
          <div class="flex mx-2">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M7 7h.01M7 3h5c.512 0 1.024.195 1.414.586l7 7a2 2 0 010 2.828l-7 7a2 2 0 01-2.828 0l-7-7A1.994 1.994 0 013 12V7a4 4 0 014-4z" />
            </svg>
            <p class="mx-2">Graphics, Wordpress</p>
          </div>
        </div>
        <div class="lg:w-8/12 md:w-full w-full pt-4">
          <p class="text-sm">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed eleifend urna eu sapien. Quisque posuere nunc eu massa. Praesent bibendum lorem non leo. Morbi volutpat, urna eu fermentum rutrum, ligula lacus interdum mauris, ac pulvinar libero pede a enim. Etiam commodo malesuada ante. Donec nec ligula. Curabitur mollis semper diam.Duis viverra nibh a felis condimentum pretium. Nullam tristique lacus non</p>
        </div>
        <div class="flex items-center pt-4">
          <svg xmlns="http://www.w3.org/2000/svg" class="h-4 text-white rounded bg-teal-500 w-4" viewBox="0 0 20 20" fill="currentColor">
            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
          </svg>
          <p class="mx-3 text-sm">Read more</p>
        </div>
      </div>
    </div>
  </div>
  <div class="pt-4">
    <div class="over_img" style="max-width:710px;">
      <img src="images/9.jpg" alt="Avatar" class="image h-auto  w-full">
      <div class="overlay items-center justify-center flex">
        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-auto w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
          <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
      </div>
    </div>
    <div class="flex pt-10">
      <div class="bg-gray-400 text-center rounded" style="height:80px;">
        <p class=" p-2 rounded">20</p>
        <p class="bg-teal-500 p-2 rounded">Apr</p>
      </div>
      <div class="mx-10 border-b-2 pb-10">
        <a href="{{url('blog-details')}}">NICE GALLERY POST</a>
        <div class="lg:flex-row md:flex-wrap flex flex-wrap mt-4 text-xs text-gray-400">
          <div class="flex ">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.354a4 4 0 110 5.292M15 21H3v-1a6 6 0 0112 0v1zm0 0h6v-1a6 6 0 00-9-5.197M13 7a4 4 0 11-8 0 4 4 0 018 0z" />
            </svg>
            <p class="mx-2">By BlueOwlCreative</p>
          </div>
          <div class="flex mx-2">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z" />
            </svg>
            <p class="mx-2">No comments yet</p>
          </div>
          <div class="flex mx-2">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M7 7h.01M7 3h5c.512 0 1.024.195 1.414.586l7 7a2 2 0 010 2.828l-7 7a2 2 0 01-2.828 0l-7-7A1.994 1.994 0 013 12V7a4 4 0 014-4z" />
            </svg>
            <p class="mx-2">Graphics, Wordpress</p>
          </div>
        </div>
        <div class="lg:w-8/12 md:w-full w-full pt-4">
          <p class="text-sm">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed eleifend urna eu sapien. Quisque posuere nunc eu massa. Praesent bibendum lorem non leo. Morbi volutpat, urna eu fermentum rutrum, ligula lacus interdum mauris, ac pulvinar libero pede a enim. Etiam commodo malesuada ante. Donec nec ligula. Curabitur mollis semper diam.Duis viverra nibh a felis condimentum pretium. Nullam tristique lacus non</p>
        </div>
        <div class="flex items-center pt-4">
          <svg xmlns="http://www.w3.org/2000/svg" class="h-4 text-white rounded bg-teal-500 w-4" viewBox="0 0 20 20" fill="currentColor">
            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
          </svg>
          <p class="mx-3 text-sm">Read more</p>
        </div>
      </div>
    </div>
  </div>
  <div class="pt-4">
    <div class="over_img " style="max-width:710px;">
      <img src="images/h3.jpg" alt="Avatar" class="image h-auto  w-full">
      <div class="overlay items-center justify-center flex text-center">
        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-auto w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
          <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
      </div>
    </div>
    <div class="flex pt-10">
      <div class="bg-gray-400 text-center rounded" style="height:80px;">
        <p class=" p-2 rounded">20</p>
        <p class="bg-teal-500 p-2 rounded">Apr</p>
      </div>
      <div class="mx-10 border-b-2 pb-10">
        <a href="{{url('blog-details')}}">FORTUNA BLOG ITEM</a>
        <div class="lg:flex-row md:flex-wrap flex flex-wrap mt-4 text-xs text-gray-400">
          <div class="flex ">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.354a4 4 0 110 5.292M15 21H3v-1a6 6 0 0112 0v1zm0 0h6v-1a6 6 0 00-9-5.197M13 7a4 4 0 11-8 0 4 4 0 018 0z" />
            </svg>
            <p class="mx-2">By BlueOwlCreative</p>
          </div>
          <div class="flex mx-2">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z" />
            </svg>
            <p class="mx-2">No comments yet</p>
          </div>
          <div class="flex mx-2">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
              <path stroke-linecap="round" stroke-linejoin="round" d="M7 7h.01M7 3h5c.512 0 1.024.195 1.414.586l7 7a2 2 0 010 2.828l-7 7a2 2 0 01-2.828 0l-7-7A1.994 1.994 0 013 12V7a4 4 0 014-4z" />
            </svg>
            <p class="mx-2">Graphics, Wordpress</p>
          </div>
        </div>
        <div class="lg:w-8/12 md:w-full w-full pt-4">
          <p class="text-sm">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed eleifend urna eu sapien. Quisque posuere nunc eu massa. Praesent bibendum lorem non leo. Morbi volutpat, urna eu fermentum rutrum, ligula lacus interdum mauris, ac pulvinar libero pede a enim. Etiam commodo malesuada ante. Donec nec ligula. Curabitur mollis semper diam.Duis viverra nibh a felis condimentum pretium. Nullam tristique lacus non</p>
        </div>
        <div class="flex items-center pt-4">
          <svg xmlns="http://www.w3.org/2000/svg" class="h-4 text-white rounded bg-teal-500 w-4" viewBox="0 0 20 20" fill="currentColor">
            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
          </svg>
          <p class="mx-3 text-sm">Read more</p>
        </div>
      </div>
    </div>
  </div>
</div>