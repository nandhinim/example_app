@extends('layouts.app')
@section('content')
<div class="bg-gray-200">
    <div class="container  mx-auto">
        <div class="flex p-10">
            <div class="">
                <p>BLOG</p>
            </div>
            <div class="flex items-center ml-auto">
                <p>Home</p>
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                </svg>
                <p>Blog</p>
            </div>
        </div>
    </div>
</div>
<div class="container mx-auto p-10">
    <div class="lg:flex-row flex md:flex-col flex-col">
        @include('.blog._left')
        @include('.blog._right')
    </div>
</div>
@endsection