<div class="lg:w-4/12 md:w-full w-full p-10">
    <div class="">
        <div class="">
            <p>CATEGORIES</p>
        </div>
        <div class="mt-6">
            <ul>
                <li class="flex">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                    </svg>
                    <p>Brand Identity</p>
                </li>
                <li class="flex mt-3">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                    </svg>
                    <p>Custom Design</p>
                </li>
                <li class="flex mt-3">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                    </svg>
                    <p>Marketing</p>
                </li>
                <li class="flex mt-3">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                    </svg>
                    <p>Mobile Apps</p>
                </li>
                <li class="flex mt-3">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                    </svg>
                    <p>Web Development</p>
                </li>
            </ul>
        </div>
        <div class="mt-8">
            <div class="">
                <p>LATEST NEWS</p>
            </div>
            <div class="flex pt-10 items-center">
                <div class="ring-2 ring-gray-300 p-2">
                    <img class="w-20 h-20" src="images/1.jpg">
                </div>
                <div class="mx-2 text-xs">
                    <p class="mb-4">LATEST NEWS POST</p>
                    <i class="text-gray-300">April 23,2015</i>
                </div>
            </div>
            <div class="flex pt-5 items-center">
                <div class="ring-2 ring-gray-300 p-2">
                    <img class="w-20 h-20" src="images/1.jpg">
                </div>
                <div class="mx-2 text-xs">
                    <p class="mb-4">LATEST NEWS POST</p>
                    <i class="text-gray-300">April 23,2015</i>
                </div>
            </div>
            <div class="flex pt-5 items-center">
                <div class="ring-2 ring-gray-300 p-2">
                    <img class="w-20 h-20" src="images/1.jpg">
                </div>
                <div class="mx-2 text-xs">
                    <p class="mb-4">LATEST NEWS POST</p>
                    <i class="text-gray-300">April 23,2015</i>
                </div>
            </div>
        </div>
    </div>
    <div class="">
        <div class="owl-slider mt-10 ">
            <p class="text-center  pt-20">WHAT CLIENT SAYS</p>
            <div id="carousel_testimonial" class="owl-carousel pt-10 " style="height:179px;">
                <div class="item lg:w-full m:w-full w-full mx-auto p-5 item-center justify-center">
                    <div class="w-9/12 mx-auto bg-gray-200 hii rounded  p-6 text-center">
                        <p>"On extremely short notice,Blue owl creative came up with the perfect design i
                            previously envisioned for my company."</p>
                    </div>
                    <style>
                        span {
                            font-size: 70px;
                            position: relative;
                            top: 64px;
                        }

                        .hii::before {
                            content: "";
                            border-right: 5px solid transparent;
                            border-bottom: 22px solid #ce3ec4;
                            border-left: 70px solid transparent;
                            right: 151px;
                            position: absolute;
                            /* bottom: -2px; */
                            top: 0px;
                            transform: rotate(-1deg);
                        }
                    </style>
                    <div class="flex items-center justify-center pt-5">
                        <img class=" h-20 w-full ring-white ring-2 rounded-full " src="images/alen.jpg" style="width: 22%;">
                    </div>
                </div>
                <div class="item lg:w-full m:w-full w-full mx-auto  p-5">
                    <div class="w-9/12 mx-auto bg-gray-200 p-6 rounded hii text-center">
                        <p>"On extremely short notice,Blue owl creative came up with the perfect design i
                            previously envisioned for my company."</p>
                    </div>
                    <div class="flex items-center justify-center pt-5">
                        <img class=" h-20 w-full ring-white ring-2 rounded-full " src="images/Alan-Smith.jpg" style="width: 22%;">
                    </div>

                </div>
                <div class="item lg:w-full m:w-full w-full mx-auto  p-5">
                    <div class="w-9/12 mx-auto bg-gray-200 rounded p-6 hii text-center">
                        <p>"On extremely short notice,Blue owl creative came up with the perfect design i
                            previously envisioned for my company."</p>
                    </div>
                    <div class="flex items-center justify-center pt-5">
                        <img class=" h-20 w-full ring-white ring-2 rounded-full " src="images/aisy.jpg" style="width: 22%;">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div x-data="{showAccordion:0}" class=" lg:w-full pt-96 md:w-full w-full mx-auto py-3">
        <div class=" my-3">
            <div @click="showAccordion = 1" class=" p-3 flex">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 cursor-pointer" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                </svg>
                <!--    <div x-text="showAccordion == 1 ? '+' : '-'"></div> -->
                <div>
                    <p class="mx-4">STUNNING WEB DESIGN</p>
                </div>
            </div>
            <div x-show="showAccordion == 1" class="p-3">
                Fortuna is a diverse WordPress theme suitable for a wide variety of websites: Corporate,
                Company,
                eCommerce, Portfolio, Personal, Blogs. Start our trial today!
            </div>
        </div>
        <div class=" my-3">
            <div @click="showAccordion = 2" class=" p-3 flex ">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 cursor-pointer w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                </svg>
                <div>
                    <p class="mx-4">HOT WORDPRESS THEMES</p>
                </div>
            </div>
            <div x-show="showAccordion == 2" class="p-3">
                Fortuna will blow your mind with the virtually unlimited customization options it offers.
                Hundreds of
                style presets are available to you for all your menus, portfolio items, post items, carousels,
                grids
                etc.
            </div>
        </div>
        <div class=" my-3">
            <div @click="showAccordion = 3" class=" p-3 flex">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 cursor-pointer w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                </svg>
                <div>
                    <p class="mx-4">BESPOKE DEVELOPMENT</p>
                </div>
            </div>
            <div x-show="showAccordion == 3" class="p-3">
                Fortuna ships with plenty of customizable interface elements ready to add to your page at the
                click of a
                button via the best Drag’n’Drop plugin – Visual Composer.
            </div>
        </div>

    </div>

</div>