<div class="lg:w-8/12 md:w-full w-full mx-auto lg:p-10 md:p-5 p-5">
    <div class="over_img" style="max-width:710px;">
        <img src="images/7.jpg" alt="Avatar" class="image h-full  w-full">
        <div class="overlay flex items-center justify-center">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 mx-auto  w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
            </svg>
        </div>
    </div>
    <div class="lg:flex-row md:flex-wrap flex flex-wrap mt-4 text-xs text-gray-400">
        <div class="flex ">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.354a4 4 0 110 5.292M15 21H3v-1a6 6 0 0112 0v1zm0 0h6v-1a6 6 0 00-9-5.197M13 7a4 4 0 11-8 0 4 4 0 018 0z" />
            </svg>
            <p class="mx-2">By BlueOwlCreative</p>
        </div>
        <div class="flex mx-2">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                <path stroke-linecap="round" stroke-linejoin="round" d="M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z" />
            </svg>
            <p class="mx-2">No comments yet</p>
        </div>
        <div class="flex mx-2">
            <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                <path stroke-linecap="round" stroke-linejoin="round" d="M7 7h.01M7 3h5c.512 0 1.024.195 1.414.586l7 7a2 2 0 010 2.828l-7 7a2 2 0 01-2.828 0l-7-7A1.994 1.994 0 013 12V7a4 4 0 014-4z" />
            </svg>
            <p class="mx-2">Graphics, Wordpress</p>
        </div>
        <div class="flex">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-4">
                <path stroke-linecap="round" stroke-linejoin="round" d="M12 6v6h4.5m4.5 0a9 9 0 11-18 0 9 9 0 0118 0z" />
            </svg>
            <p class="mx-2">April 23,2015</p>
        </div>
    </div>
    <div class="pt-5">
        <div class="">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed eleifend urna eu sapien. Quisque posuere nunc eu massa. Praesent bibendum lorem non leo. Morbi volutpat, urna eu fermentum rutrum, ligula lacus interdum mauris, ac pulvinar libero pede a enim. Etiam commodo malesuada ante. Donec nec ligula. Curabitur mollis semper diam.Duis viverra nibh a felis condimentum pretium. Nullam tristique lacus non purus. Donec vel felis. Etiam et sapien. Pellentesque nec quam a justo
                tincidunt laoreet. Aenean id enim. Donec lorem arcu, eleifend venenatis, rhoncus mollis, semper at, dui.</p>
        </div>
        <div class="pt-5">
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed eleifend urna eu sapien. Quisque posuere nunc eu massa. Praesent bibendum lorem non leo. Morbi volutpat, urna eu fermentum rutrum, ligula lacus interdum mauris, ac pulvinar libero pede a enim. Etiam commodo malesuada ante. Donec nec ligula. Curabitur mollis semper diam.Duis viverra nibh a felis condimentum pretium. Nullam tristique lacus non purus. Donec vel felis. Etiam et sapien. Pellentesque nec quam a justo tincidunt laoreet.
                Aenean id enim. Donec lorem arcu, eleifend venenatis, rhoncus mollis, semper at, dui.</p>
        </div>
        <div class="pt-5">
            <p>Aenean id orci. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus
                magna. Mauris tincidunt iaculis enim. Duis a mi vitae sapien dapibus tincidunt. Proin metus.</p>
        </div>
        <div class="pt-5">
            <p>Proin cursus, libero non auctor faucibus, urna mi vestibulum orci, sit amet fermentum nibh purus eget enim. Aenean aliquet ligula nec nulla. Praesent sit amet lorem vitae massa hendrerit auctor. Sed sit amet urna. Aenean sapien nunc, imperdiet a, pharetra in, consequat eu, neque. Phasellus vel
                sem gravida augue consequat tempor. Curabitur eget mauris at pede varius facilisis.</p>
        </div>
        <div class="pt-5">
            <p>Morbi ut sapien. Morbi arcu mauris, suscipit congue, placerat sit amet, suscipit a, ante. Donec aliquet dui ac nunc. Mauris magna quam, aliquet quis, viverra eu, fringilla eget, purus. Donec tristique pretium sem.</p>
        </div>
    </div>
    <div class="">
        <div class="pt-10">
            <p>LEAVE A COMMENT</p>
        </div>
        <div class="">
            <ul>
                <li class="mt-4">
                    <p class="text-xs">Comment <span class="text-red-500" style="top:0;font-size:12px;">*</span></p>
                    <textarea class="border-2 mt-1 bg-gray-100 px-2" cols="30" rows="5"></textarea>
                </li>
                <li>
                    <p class="text-xs mt-5">Name <span class="text-red-500" style="top:0;font-size:12px;">*</span></p>
                    <input type="text" class="border-2 bg-gray-100 px-2 lg:w-6/12 md:w-full w-full h-10 mt-1" name="name">
                </li>
                <li>
                    <p class="text-xs mt-5">Email<span class="text-red-500" style="top:0;font-size:12px;">*</span></p>
                    <input type="email" class="border-2 bg-gray-100 px-2 h-10 lg:w-6/12 md:w-full w-full mt-1">
                </li>
                <li>
                    <p class="text-xs mt-5" style="top:0;font-size:12px;">Website</p>
                    <input type="url" class="border-2 bg-gray-100 px-2 h-10 lg:w-6/12 md:w-full w-full mt-1">
                </li>
                <li class="mt-10">
                    <a href="#" class="bg-teal-500 px-6 py-2 text-white rounded">COMMENT</a>
                </li>
            </ul>
        </div>
    </div>
</div>