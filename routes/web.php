<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/slide_bar', function () {
    return view('slide_bar.sli');
});
Route::get('/contact', function () {
    return view('contact');
});
Route::get('/blog', function () {
    return view('blog');
});
Route::get('/shop', function () {
    return view('shop');
});
Route::get('/details', function () {
    return view('details');
});
Route::get('/blog-details', function () {
    return view('blog-details');
});
Route::get('/charts', function () {
    return view('charts');
});
Route::get('/boxchart', function () {
    return view('boxcharts');
});
Route::get('/linechart', function () {
    return view('linechart');
});
Route::get('/roundchart', function () {
    return view('roundchart');
});
Route::get('/sidebar', function () {
    return view('sidebars');
});
Route::get('/accordion', function () {
    return view('accordion');
});
Route::get('/isotope', function () {
    return view('isotopefilter');
});
Route::get('/tabs_accordion', function () {
    return view('alphinejs');
});
Route::get('/jquerytabs', function () {
    return view('jquerytabs');
});
Route::get('/thumbnail_sliders', function () {
    return view('thumbnailsliders');
});
Route::get('/profileslider', function () {
    return view('profileslider');
});
Route::get('/alphinejstag', function () {
    return view('alphinejstag');
});
Route::get('/slowcarousel', function () {
    return view('slowcarousel');
});
Route::get('/table', function () {
    return view('table');
});
Route::get('/grid', function () {
    return view('grid');
});
Route::get('/drag_the_content', function () {
    return view('drag');
});
Route::get('/Move Items Between Two Select Lists Using jQuery', function () {
    return view('list_transfer');
});
Route::get('/texteditor', function () {
    return view('texteditor');
});
Route::get('/full_calendar', function () {
    return view('fullcalendar');
});
